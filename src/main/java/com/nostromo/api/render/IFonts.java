package com.nostromo.api.render;

import com.kilo.TrueTypeFont;

public interface IFonts {

    TrueTypeFont loadTTF(String path, int style, float size);
}
