package com.nostromo.api.render;

import com.kilo.TrueTypeFont;
import com.kilo.utilimport.Align;
import net.minecraft.entity.EntityLivingBase;
import org.newdawn.slick.opengl.Texture;

public interface IDraw {

    void startClip(float x1, float y1, float x2, float y2);

    void endClip();

    void rect(float x1, float y1, float x2, float y2, int fill);

    void rectBorder(float x1, float y1, float x2, float y2, int outline);

    void rectGradient(float x1, float y1, float x2, float y2, int startColor,int endColor);

    void line(float x1, float y1, float x2, float y2, int color, float width);

    void triangle(float x1, float y1, float x2, float y2, float x3, float y3, int fill);

    void circle(float x, float y, float radius, int fill);

    void ellipse(float x, float y, float w, float h, int fill);

    void point(float x, float y, float size, int fill);

    void stringShadow(TrueTypeFont font, float x, float y, String text, int color, Align hAlign, Align vAlign);

    void string(TrueTypeFont font, float x, float y, String text, int color, Align hAlign, Align vAlign, boolean shadow);

    void arc(float x, float y, float start, float end, float radius, int color);

    void arcEllipse(float x, float y, float start, float end, float h, float w, int color);

    void polygon(float x, float y, float[] xPoints, float[] yPoints, int fill);

    void rectTexture(float x, float y, float w, float h, Texture texture, int color);

    void rectTexture(float x, float y, float w, float h, Texture texture, float opacity);

    void drawEntityOnScreen(int x, int y, int z, float f1, float f2, EntityLivingBase entity, int color);

    void loader(float x, float y, float gap, int color);
}
