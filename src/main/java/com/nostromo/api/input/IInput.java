package com.nostromo.api.input;

public interface IInput {

    void handle();
}
