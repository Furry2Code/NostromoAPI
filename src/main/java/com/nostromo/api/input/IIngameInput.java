package com.nostromo.api.input;

public interface IIngameInput {

    void keyboardPress(int key);

    void keyboardRelease(int key);
}
