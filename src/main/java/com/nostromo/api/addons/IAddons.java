package com.nostromo.api.addons;

public interface IAddons {

    void onEnable();

    void onDisable();

    void update();
}
