package com.nostromo.api.encryption;

public interface ICipherFiles {

    String startEncode(String inputs);

    byte[] encrypt(byte[] input);

    String decrypt(byte[] input);
}
