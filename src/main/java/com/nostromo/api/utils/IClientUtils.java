package com.nostromo.api.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.multiplayer.WorldClient;

public interface IClientUtils {

    Minecraft mc();

    EntityPlayerSP player();

    WorldClient world();

}
