package com.nostromo.api.utils;

import net.minecraft.entity.Entity;
import net.minecraft.util.Vec3;

import java.awt.*;

public interface IRenderUtil {

    double[] entityRenderPos(Entity entity);

    double[] entityWorldPos(Entity entity);

    double[] renderPos(Vec3 vector);

    boolean transparentEntity(int id);

    boolean renderFar();

    String hsvToRgb(float hue, float saturation, float value);

    String rgbToString(float r, float g, float b);

    Color getRainbow(float fade);

    void setupFarRender();

    void enableLighting();

    void disableLighting();

}
