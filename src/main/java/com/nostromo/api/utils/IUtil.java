package com.nostromo.api.utils;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.BlockPos;

import java.util.List;

public interface IUtil {
    boolean isASCII(char c);

    int reAlpha(int color, float alpha);

    int blendColor(int color1, int color2, float perc);

    String replaceFormat(String s);

    void openWeb(String s);

    List<String> usernameHistory(String username);

    String getUUIDFromName(String username);

    List<String> getNamesFromUUID(String uuid);

    double safeDiv(double x, double y, double z);

    String prettyFloat(double f);

    float angleDifference(float a, float b);

    float makeFloat(Object o);

    int makeInteger(Object o);

    boolean makeBoolean(Object o);

    boolean canSeeEntity(EntityLivingBase from, EntityLivingBase to);

    float[] getRotationToBlockPos(BlockPos bp);

    float[] getRotationToPos(double x, double y, double z);
}
