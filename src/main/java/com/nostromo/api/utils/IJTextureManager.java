package com.nostromo.api.utils;

import com.kilo.render.JTextureImage;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public interface IJTextureManager {
    List<JTextureImage> cache = new CopyOnWriteArrayList<JTextureImage>();

    boolean exists(String location);

    JTextureImage get(String location);
}
