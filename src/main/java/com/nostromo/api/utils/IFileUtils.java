package com.nostromo.api.utils;

import java.io.File;
import java.util.List;

public interface IFileUtils {

    List<String> read(File inputFile);

    void write(File output, List<String> writeContent, boolean overwriteContent);

    File getConfigFile(String filename);
}
