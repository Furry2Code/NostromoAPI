package com.nostromo.api.utils;

import net.minecraft.entity.EntityLivingBase;

import java.util.List;

public interface IRotationUtils {

    float[] getRotations(EntityLivingBase ent);

    float[] getAverageRotations(List<EntityLivingBase> targetList);

    float[] getRotationFromPosition(double x, double z, double y);

    float getTrajAngleSolutionLow(float d3, float d1, float velocity);

    float getNewAngle(float angle);

    float getDistanceBetweenAngles(float angle1, float angle2);
}
