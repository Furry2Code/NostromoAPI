package com.nostromo.api.utils;

import java.util.UUID;

public interface IUUIDUtil {
    String getUUIDWithoutDashes();

    UUID getClientUUID();
}
