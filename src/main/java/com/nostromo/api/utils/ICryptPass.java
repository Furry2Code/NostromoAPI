package com.nostromo.api.utils;

public interface ICryptPass {
    String salt = "4356TYUHJGNBV5RTJGHN547E567575bvnxdfgbvc4356TY54g";

    String toSHA1(String text);

    String toSHA256(String text);

    String toMD5(String text);

    String crypt(String text, String type);

    boolean compareHash(String text, String hash);
}
