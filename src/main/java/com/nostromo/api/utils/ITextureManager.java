package com.nostromo.api.utils;

import com.kilo.render.TextureImage;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public interface ITextureManager {
    List<TextureImage> cache = new CopyOnWriteArrayList<TextureImage>();

    boolean exists(String location);

    TextureImage get(String location);

    List<TextureImage> getCaches();
}
