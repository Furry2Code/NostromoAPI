package com.nostromo.api.utils;

public interface ITimeConverter {

    String convertTime(long time);
}
