package com.nostromo.api.utils;

import com.kilo.render.JTextureImage;
import com.kilo.render.TextureImage;

import java.io.IOException;
import java.io.InputStream;

public interface IResources {

    void loadWithThread();

    InputStream _load(String name) throws IOException;

    InputStream _load(InputStream stream) throws IOException;

    void loadTextures() throws Exception;

    void loadBackground(boolean main) throws Exception;

    void bindTextures() throws Exception;

    void loadUserTexture();

    TextureImage downloadTexture(final String imageURL);

    JTextureImage jDownloadTexture(int index, final String imageURL);

    void onTickChange();
}
