package com.nostromo.api.utils;

import java.awt.image.BufferedImage;

public interface IKiloCapeUtils {

    BufferedImage parseCape(final BufferedImage img);
}
