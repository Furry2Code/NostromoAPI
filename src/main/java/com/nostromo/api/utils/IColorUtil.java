package com.nostromo.api.utils;

import com.kilo.utilimport.ChatColor;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public interface IColorUtil {
    Map<String, ChatColor> colors = new HashMap<String, ChatColor>();

    int transparency(int color, double alpha);

    Color rainbow(long offset, float fade);

    float[] getRGBA(int color);

    int intFromHex(String hex);

    String hexFromInt(int color);

    Color blend(Color color, Color color2, Double ratio);

    Color darker(Color color, double fraction);

    Color lighter(Color color, double fraction);

    String getHexName(Color color);

    double colorDistance(double r1, double g1, double b1, double r2, double g2, double b2);

    double colorDistance(double[] color1, double[] color2);

    double colorDistance(Color color1, Color color2);

    boolean isDark(double r, double g, double b);

    boolean isDark(Color color);

    HashMap<String, ChatColor> getColors();
}
