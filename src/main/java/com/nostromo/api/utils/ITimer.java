package com.nostromo.api.utils;

import java.util.concurrent.TimeUnit;

public interface ITimer {
    Long previousTime = 0L;
    Long prevMS = 0L;

    boolean hasReach(long time, TimeUnit timeUnit);

    boolean isTime(Float time);

    float currentTime();

    void reset();

    long systemTime();

    boolean delay(float milliSec);
}
