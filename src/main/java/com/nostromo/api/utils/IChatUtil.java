package com.nostromo.api.utils;

import com.kilo.TrueTypeFont;
import com.kilo.renderimport.Format;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import org.newdawn.slick.Color;


public interface IChatUtil {

    boolean isAllowedCharacter(char character);

    boolean isValidUsername(String line);

    void send(String message);

    String addFormat(String message, String regex);

    IChatComponent makeComponent(String message);

    EnumChatFormatting charToFormat(char c);

    boolean isValidEmail(String line);

    boolean validateEmail(String hex);

    boolean isNumber(String line);

    String insertAt(String line, String character, int index);

    String replaceAt(String line, String character, int start, int end);

    int getMousePos(String text, TrueTypeFont font, float x, int mx, float scroll);

    Format getChatFormatter(char c);

    Color getColorFromChar(char c);

    String clearFormat(String s);
}
