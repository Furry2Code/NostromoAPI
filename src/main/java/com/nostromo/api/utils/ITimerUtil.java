package com.nostromo.api.utils;

import java.util.concurrent.TimeUnit;

public interface ITimerUtil {
    Long previousTime = 0L;
    Long currentMS = 0L;
    Long lastMS = 0L;
    Long prevMS = 0L;

    boolean delay(Float milliSec);

    boolean isDelayComplete(long delay);

    long systemTime();

    void update();

    float currentTime();

    boolean isTime(float time);

    void reset();

    boolean hasPassed(long MS);

    boolean hasReach(long a, TimeUnit milliseconds);
}
