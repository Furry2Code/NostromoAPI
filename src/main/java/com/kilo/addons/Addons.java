package com.kilo.addons;

import com.nostromo.api.addons.IAddons;

public abstract class Addons implements IAddons {
    private String name;
    private String addons_id;
    private String version;
    private String author;
    public EnumCategory category;

    public Addons(String name, String addons_id, String version, String author, EnumCategory category) {
        this.name = name;
        this.addons_id = addons_id;
        this.version = version;
        this.author = author;
        this.category = category;
    }
}
