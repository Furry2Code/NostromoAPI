package com.kilo.render

import com.kilo.API
import com.kilo.util.Util
import com.kilo.utilimport.Align
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.client.renderer.RenderHelper
import net.minecraft.client.renderer.Tessellator
import net.minecraft.entity.EntityLivingBase
import org.lwjgl.opengl.Display
import org.lwjgl.opengl.GL11
import org.newdawn.slick.Color
import com.kilo.TrueTypeFont
import com.nostromo.api.render.IDraw
import org.newdawn.slick.opengl.Texture
import org.newdawn.slick.opengl.TextureImpl
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin

class Draw : IDraw {
    val instance: Draw = this
    private val mc = Minecraft.getMinecraft()

    override fun startClip(x1: Float, y1: Float, x2: Float, y2: Float) {
        var y1 = y1
        var y2 = y2
        val temp: Float
        if (y1 > y2) {
            temp = y2
            y2 = y1
            y1 = temp
        }
        GL11.glScissor(
            x1.toInt(),
            (Display.getHeight() - y2).toInt(),
            (x2 - x1).toInt(),
            (y2 - y1).toInt()
        )
        GL11.glEnable(GL11.GL_SCISSOR_TEST)
    }

    override fun endClip() {
        GL11.glDisable(GL11.GL_SCISSOR_TEST)
    }

    override fun rect(x1: Float, y1: Float, x2: Float, y2: Float, fill: Int) {
        var x1 = x1
        var y1 = y1
        var x2 = x2
        var y2 = y2
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        var var5: Float
        if (x1 < x2) {
            var5 = x1
            x1 = x2
            x2 = var5
        }
        if (y1 < y2) {
            var5 = y1
            y1 = y2
            y2 = var5
        }
        val var11 = (fill shr 24 and 255).toFloat() / 255.0f
        val var6 = (fill shr 16 and 255).toFloat() / 255.0f
        val var7 = (fill shr 8 and 255).toFloat() / 255.0f
        val var8 = (fill and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val var10 = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.color(var6, var7, var8, var11)
        var10.startDrawingQuads()
        var10.addVertex(x1.toDouble(), y2.toDouble(), 0.0)
        var10.addVertex(x2.toDouble(), y2.toDouble(), 0.0)
        var10.addVertex(x2.toDouble(), y1.toDouble(), 0.0)
        var10.addVertex(x1.toDouble(), y1.toDouble(), 0.0)
        var9.draw()
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    override fun rectBorder(x1: Float, y1: Float, x2: Float, y2: Float, outline: Int) {
        rect(x1, y2 - 1, x2, y2, outline)
        rect(x1, y1, x2, y1 + 1, outline)
        rect(x1, y1, x1 + 1, y2, outline)
        rect(x2 - 1, y1, x2, y2, outline)
    }

    override fun rectGradient(
        x1: Float,
        y1: Float,
        x2: Float,
        y2: Float,
        startColor: Int,
        endColor: Int
    ) {
        val var7 = (startColor shr 24 and 255) / 255.0f
        val var8 = (startColor shr 16 and 255) / 255.0f
        val var9 = (startColor shr 8 and 255) / 255.0f
        val var10 = (startColor and 255) / 255.0f
        val var11 = (endColor shr 24 and 255) / 255.0f
        val var12 = (endColor shr 16 and 255) / 255.0f
        val var13 = (endColor shr 8 and 255) / 255.0f
        val var14 = (endColor and 255) / 255.0f
        GlStateManager.disableTexture2D()
        GlStateManager.enableBlend()
        GlStateManager.disableAlpha()
        GlStateManager.disableDepth()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.shadeModel(7425)
        val var15 = Tessellator.getInstance()
        val var16 = var15.worldRenderer
        var16.startDrawingQuads()
        var16.setColorRGBA_F(var8, var9, var10, var7)
        var16.addVertex(x2.toDouble(), y1.toDouble(), 0.0)
        var16.addVertex(x1.toDouble(), y1.toDouble(), 0.0)
        var16.setColorRGBA_F(var12, var13, var14, var11)
        var16.addVertex(x1.toDouble(), y2.toDouble(), 0.0)
        var16.addVertex(x2.toDouble(), y2.toDouble(), 0.0)
        var15.draw()
        GlStateManager.shadeModel(7424)
        GlStateManager.disableBlend()
        GlStateManager.enableAlpha()
        GlStateManager.enableDepth()
        GlStateManager.enableTexture2D()
    }

    override fun line(
        x1: Float,
        y1: Float,
        x2: Float,
        y2: Float,
        color: Int,
        width: Float
    ) {
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        GL11.glLineWidth(width)
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val var10 = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.color(var6, var7, var8, var11)
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        var10.startDrawing(1)
        var10.addVertex(x1.toDouble(), y1.toDouble(), 0.0)
        var10.addVertex(x2.toDouble(), y2.toDouble(), 0.0)
        var9.draw()
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    override fun triangle(
        x1: Float,
        y1: Float,
        x2: Float,
        y2: Float,
        x3: Float,
        y3: Float,
        fill: Int
    ) {
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        val var11 = (fill shr 24 and 255).toFloat() / 255.0f
        val var6 = (fill shr 16 and 255).toFloat() / 255.0f
        val var7 = (fill shr 8 and 255).toFloat() / 255.0f
        val var8 = (fill and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val var10 = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.color(var6, var7, var8, var11)
        GL11.glBegin(GL11.GL_TRIANGLE_FAN)
        GL11.glVertex2f(x1, y1)
        GL11.glVertex2f(x3, y3)
        GL11.glVertex2f(x2, y2)
        GL11.glVertex2f(x1, y1)
        GL11.glEnd()
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    override fun circle(x: Float, y: Float, radius: Float, fill: Int) {
        arc(x, y, 0f, 360f, radius - 1, fill)
    }

    override fun ellipse(x: Float, y: Float, w: Float, h: Float, fill: Int) {
        arcEllipse(x, y, 0f, 360f, w - 1, h - 1, fill)
    }

    override fun point(x: Float, y: Float, size: Float, fill: Int) {
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        val var11 = (fill shr 24 and 255).toFloat() / 255.0f
        val var6 = (fill shr 16 and 255).toFloat() / 255.0f
        val var7 = (fill shr 8 and 255).toFloat() / 255.0f
        val var8 = (fill and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val var10 = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.color(var6, var7, var8, var11)
        GL11.glEnable(GL11.GL_POINT_SMOOTH)
        GL11.glPointSize(size)
        GL11.glBegin(GL11.GL_POINTS)
        GL11.glVertex2f(x, y)
        GL11.glEnd()
        GL11.glDisable(GL11.GL_POINT_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    override fun stringShadow(
        font: TrueTypeFont,
        x: Float,
        y: Float,
        text: String?,
        color: Int,
        hAlign: Align?,
        vAlign: Align?
    ) {
        val opacity = Math.max((color shr 24 and 255) / 255f, 0.05f)
        string(
            font,
            x + 1,
            y + 1,
            text,
            API.util.reAlpha(-0x1000000, opacity),
            hAlign,
            vAlign,
            true
        )
        string(font, x, y, text, API.util.reAlpha(color, opacity), hAlign, vAlign, false)
    }

    override fun string(
        font: TrueTypeFont,
        x: Float,
        y: Float,
        text: String?,
        color: Int,
        hAlign: Align?,
        vAlign: Align?,
        shadow: Boolean
    ) {
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        var offsetX = 0
        var offsetY = 0
        when (hAlign) {
            Align.C -> offsetX = -font.getWidth(text) / 2
            Align.R -> offsetX = -font.getWidth(text)
            else -> {
            }
        }
        when (vAlign) {
            Align.C -> offsetY = -font.getHeight(text) / 2
            Align.B -> offsetY = -font.getHeight(text)
            else -> {
            }
        }
       // mc.textureManager.bindTexture(mc.fontRendererObj.locationFontTextureBase)
        GlStateManager.enableBlend()
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
        TextureImpl.bindNone()
        font.drawString(
            (x + offsetX).roundToInt().toFloat(),
            (y + offsetY).roundToInt().toFloat(),
            text,
            Color(color),
            shadow
        )
        GlStateManager.disableBlend()
    }

    override fun arc(
        x: Float,
        y: Float,
        start: Float,
        end: Float,
        radius: Float,
        color: Int
    ) {
        arcEllipse(x, y, start, end, radius, radius, color)
    }

    override fun arcEllipse(
        x: Float,
        y: Float,
        start: Float,
        end: Float,
        w: Float,
        h: Float,
        color: Int
    ) {
        var start = start
        var end = end
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        var temp = 0f
        if (start > end) {
            temp = end
            end = start
            start = temp
        }
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val var10 = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.color(var6, var7, var8, var11)
        if (var11 > 0.5f) {
            GL11.glEnable(GL11.GL_LINE_SMOOTH)
            GL11.glLineWidth(2f)
            GL11.glBegin(GL11.GL_LINE_STRIP)
            var i = end
            while (i >= start) {
                val ldx = Math.cos(i * Math.PI / 180).toFloat() * (w * 1.001f)
                val ldy = Math.sin(i * Math.PI / 180).toFloat() * (h * 1.001f)
                GL11.glVertex2f(x + ldx, y + ldy)
                i -= (360 / 90).toFloat()
            }
            GL11.glEnd()
            GL11.glDisable(GL11.GL_LINE_SMOOTH)
        }
        GL11.glBegin(GL11.GL_TRIANGLE_FAN)
        var i = end
        while (i >= start) {
            val ldx = Math.cos(i * Math.PI / 180).toFloat() * w
            val ldy = Math.sin(i * Math.PI / 180).toFloat() * h
            GL11.glVertex2f(x + ldx, y + ldy)
            i -= (360 / 90).toFloat()
        }
        GL11.glEnd()
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    override fun polygon(
        x: Float,
        y: Float,
        xPoints: FloatArray,
        yPoints: FloatArray,
        fill: Int
    ) {
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        val var11 = (fill shr 24 and 255).toFloat() / 255.0f
        val var6 = (fill shr 16 and 255).toFloat() / 255.0f
        val var7 = (fill shr 8 and 255).toFloat() / 255.0f
        val var8 = (fill and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val var10 = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.color(var6, var7, var8, var11)
        GL11.glBegin(GL11.GL_TRIANGLE_FAN)
        for (i in xPoints.indices.reversed()) {
            GL11.glVertex2f(x + xPoints[i], y + yPoints[i])
        }
        GL11.glEnd()
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    override fun rectTexture(
        x: Float,
        y: Float,
        w: Float,
        h: Float,
        texture: Texture?,
        color: Int
    ) {
        var x = x
        var y = y
        var w = w
        var h = h
        if (texture == null) {
            return
        }
        GlStateManager.color(0f, 0f, 0f)
        GL11.glColor4f(0f, 0f, 0f, 0f)
        x = Math.round(x).toFloat()
        w = Math.round(w).toFloat()
        y = Math.round(y).toFloat()
        h = Math.round(h).toFloat()
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        GlStateManager.color(var6, var7, var8, var11)
        GL11.glEnable(GL11.GL_BLEND)
        GL11.glEnable(GL11.GL_TEXTURE_2D)
        texture.bind()
        val tw = w / texture.textureWidth / (w / texture.imageWidth)
        val th = h / texture.textureHeight / (h / texture.imageHeight)
        GL11.glBegin(GL11.GL_QUADS)
        GL11.glTexCoord2f(0f, 0f)
        GL11.glVertex2f(x, y)
        GL11.glTexCoord2f(0f, th)
        GL11.glVertex2f(x, y + h)
        GL11.glTexCoord2f(tw, th)
        GL11.glVertex2f(x + w, y + h)
        GL11.glTexCoord2f(tw, 0f)
        GL11.glVertex2f(x + w, y)
        GL11.glEnd()
        GL11.glDisable(GL11.GL_TEXTURE_2D)
        GL11.glDisable(GL11.GL_BLEND)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    override fun rectTexture(
        x: Float,
        y: Float,
        w: Float,
        h: Float,
        texture: Texture?,
        opacity: Float
    ) {
        rectTexture(x, y, w, h, texture, API.util.reAlpha(Colors.WHITE.c, opacity))
    }

    override fun drawEntityOnScreen(
        p_147046_0_: Int,
        p_147046_1_: Int,
        p_147046_2_: Int,
        p_147046_3_: Float,
        p_147046_4_: Float,
        p_147046_5_: EntityLivingBase,
        color: Int
    ) {
        val a = (color shr 24 and 255).toFloat() / 255.0f
        val r = (color shr 16 and 255).toFloat() / 255.0f
        val g = (color shr 8 and 255).toFloat() / 255.0f
        val b = (color and 255).toFloat() / 255.0f
        GlStateManager.color(r, g, b, a)
        GlStateManager.enableDepth()
        GlStateManager.depthMask(true)
        GlStateManager.enableColorMaterial()
        GlStateManager.pushMatrix()
        GlStateManager.translate(p_147046_0_.toFloat(), p_147046_1_.toFloat(), 50.0f)
        GlStateManager.scale((-p_147046_2_).toFloat(), p_147046_2_.toFloat(), p_147046_2_.toFloat())
        GlStateManager.rotate(180.0f, 0.0f, 0.0f, 1.0f)
        GlStateManager.rotate(135.0f, 0.0f, 1.0f, 0.0f)
        RenderHelper.enableStandardItemLighting()
        GlStateManager.rotate(-135.0f, 0.0f, 1.0f, 0.0f)
        GlStateManager.rotate(
            -Math.atan((0 / 40.0f).toDouble()).toFloat() * 20.0f,
            1.0f,
            0.0f,
            0.0f
        )
        p_147046_5_.renderYawOffset = Math.atan((0 / 40.0f).toDouble()).toFloat() * 20.0f
        p_147046_5_.rotationYaw = Math.atan((p_147046_3_ / 40.0f).toDouble()).toFloat() * 40.0f
        p_147046_5_.rotationPitch =
            -Math.atan((p_147046_4_ / 40.0f).toDouble()).toFloat() * 20.0f
        p_147046_5_.rotationYawHead = p_147046_5_.rotationYaw
        p_147046_5_.prevRotationYawHead = p_147046_5_.rotationYaw
        val var11 = Minecraft.getMinecraft().renderManager
        var11.setPlayerViewY(180.0f)
        var11.isRenderShadow = false
        var11.renderEntityWithPosYaw(p_147046_5_, 0.0, 0.0, 0.0, 0.0f, 1.0f)
        var11.isRenderShadow = true
        GlStateManager.popMatrix()
        RenderHelper.disableStandardItemLighting()
        GlStateManager.disableRescaleNormal()
        GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit)
        GlStateManager.disableTexture2D()
        GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit)
        GlStateManager.disableDepth()
        GlStateManager.disableColorMaterial()
    }

    override fun loader(x: Float, y: Float, gap: Float, color: Int) {
        val rot = System.nanoTime() / 5000000f % 360f
        var i = 0
        while (i < 360) {
            val xx = cos((i + rot) * Math.PI / 180).toFloat() * gap
            val yy = sin((i + rot) * Math.PI / 180).toFloat() * gap
            circle(x + xx, y + yy, gap / 4, color)
            i += 360 / 7f.toInt()
        }
    }
}