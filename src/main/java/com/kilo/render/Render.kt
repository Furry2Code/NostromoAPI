package com.kilo.render

import com.kilo.API
import com.kilo.util.RenderUtil
import com.kilo.util.Util
import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.Tessellator
import net.minecraft.entity.Entity
import net.minecraft.util.AxisAlignedBB
import net.minecraft.util.BlockPos
import net.minecraft.util.EnumFacing
import net.minecraft.util.MovingObjectPosition.MovingObjectType
import net.minecraft.util.Vec3
import org.lwjgl.opengl.GL11
import javax.vecmath.Vector3d

object Render {
    private val mc = Minecraft.getMinecraft()
    fun box(e: Entity, color: Int) {
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val t = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        GlStateManager.color(var6, var7, var8, var11)
        val pos = API.getRenderUtil().entityRenderPos(e)
        val bb = e.entityBoundingBox
        val aa = AxisAlignedBB(
            bb.minX - e.posX + pos!![0],
            bb.minY - e.posY + pos[1],
            bb.minZ - e.posZ + pos[2],
            bb.maxX - e.posX + pos[0],
            bb.maxY - e.posY + pos[1],
            bb.maxZ - e.posZ + pos[2]
        )
        val draw = 7
        cubeFill(
            draw,
            aa.minX,
            aa.minY,
            aa.minZ,
            aa.maxX,
            aa.maxY,
            aa.maxZ,
            floatArrayOf(0f, -e.rotationYaw, 0f),
            booleanArrayOf(true, true, true, true, true, true)
        )
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    fun boxOutline(e: Entity, color: Int) {
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val t = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        GL11.glLineWidth(1f)
        GlStateManager.color(var6, var7, var8, var11)
        val pos = API.getRenderUtil().entityRenderPos(e)
        val bb = e.entityBoundingBox
        val aa = AxisAlignedBB(
            bb.minX - e.posX + pos[0],
            bb.minY - e.posY + pos[1],
            bb.minZ - e.posZ + pos[2],
            bb.maxX - e.posX + pos[0],
            bb.maxY - e.posY + pos[1],
            bb.maxZ - e.posZ + pos[2]
        )
        val draw = 1
        cubeOutline(
            draw,
            aa.minX,
            aa.minY,
            aa.minZ,
            aa.maxX,
            aa.maxY,
            aa.maxZ,
            floatArrayOf(0f, -e.rotationYaw, 0f),
            booleanArrayOf(true, true, true, true, true, true)
        )
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    fun boxFilled(e: Entity, outline: Int, fill: Int) {
        box(e, outline)
        boxOutline(e, fill)
    }

    fun block(bp: BlockPos, color: Int, drawMode: Int, faces: BooleanArray) {
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        GL11.glLineWidth(1f)
        GlStateManager.color(var6, var7, var8, var11)
        val aa = AxisAlignedBB(
            bp,
            bp.offset(EnumFacing.SOUTH).offset(EnumFacing.EAST).offset(EnumFacing.UP)
        ).offset(
            -mc.renderManager.viewerPosX,
            -mc.renderManager.viewerPosY,
            -mc.renderManager.viewerPosZ
        )
        if (drawMode == 7) {
            cubeFill(
                drawMode,
                aa.minX,
                aa.minY,
                aa.minZ,
                aa.maxX,
                aa.maxY,
                aa.maxZ,
                floatArrayOf(0f, 0f, 0f),
                faces
            )
        } else {
            cubeOutline(
                drawMode,
                aa.minX,
                aa.minY,
                aa.minZ,
                aa.maxX,
                aa.maxY,
                aa.maxZ,
                floatArrayOf(0f, 0f, 0f),
                faces
            )
        }
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    fun blockBB(
        b: Block,
        bp: BlockPos?,
        expand: Double,
        color: Int,
        drawMode: Int,
        faces: BooleanArray
    ) {
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        GL11.glLineWidth(1f)
        GlStateManager.color(var6, var7, var8, var11)
        val aa = b.getSelectedBoundingBox(Minecraft.theWorld, bp).offset(
            -mc.renderManager.viewerPosX,
            -mc.renderManager.viewerPosY,
            -mc.renderManager.viewerPosZ
        ).expand(expand, expand, expand)
        if (drawMode == 7) {
            cubeFill(
                drawMode,
                aa.minX,
                aa.minY,
                aa.minZ,
                aa.maxX,
                aa.maxY,
                aa.maxZ,
                floatArrayOf(0f, 0f, 0f),
                faces
            )
        } else {
            cubeOutline(
                drawMode,
                aa.minX,
                aa.minY,
                aa.minZ,
                aa.maxX,
                aa.maxY,
                aa.maxZ,
                floatArrayOf(0f, 0f, 0f),
                faces
            )
        }
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    fun blockBox(bp: BlockPos, outline: Int, fill: Int, faces: BooleanArray) {
        block(bp, fill, 1, faces)
        block(bp, outline, 7, faces)
    }

    @kotlin.jvm.JvmStatic
    fun blockBoxBB(
        b: Block,
        bp: BlockPos?,
        expand: Double,
        outline: Int,
        fill: Int,
        faces: BooleanArray
    ) {
        blockBB(b, bp, expand, fill, 1, faces)
        blockBB(b, bp, expand, outline, 7, faces)
    }

    fun bbox(pos1: Vec3, pos2: Vec3, outline: Int, fill: Int) {
        val ovar11 = (outline shr 24 and 255).toFloat() / 255.0f
        val ovar6 = (outline shr 16 and 255).toFloat() / 255.0f
        val ovar7 = (outline shr 8 and 255).toFloat() / 255.0f
        val ovar8 = (outline and 255).toFloat() / 255.0f
        val fvar11 = (fill shr 24 and 255).toFloat() / 255.0f
        val fvar6 = (fill shr 16 and 255).toFloat() / 255.0f
        val fvar7 = (fill shr 8 and 255).toFloat() / 255.0f
        val fvar8 = (fill and 255).toFloat() / 255.0f
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        GL11.glLineWidth(1f)
        val p1 = API.getRenderUtil().renderPos(pos1)
        val p2 = API.getRenderUtil().renderPos(pos2)
        GlStateManager.color(fvar6, fvar7, fvar8, fvar11)
        cubeFill(
            7,
            p1!![0],
            p1[1],
            p1[2],
            p2[0] + 1,
            p2[1] + 1,
            p2[2] + 1,
            floatArrayOf(0f, 0f, 0f),
            booleanArrayOf(true, true, true, true, true, true)
        )
        GlStateManager.color(ovar6, ovar7, ovar8, ovar11)
        cubeOutline(
            1,
            p1[0],
            p1[1],
            p1[2],
            p2[0] + 1,
            p2[1] + 1,
            p2[2] + 1,
            floatArrayOf(0f, 0f, 0f),
            booleanArrayOf(true, true, true, true, true, true)
        )
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    fun tracer(from: Entity?, to: Entity, color: Int) {
        GlStateManager.loadIdentity()
        mc.entityRenderer.orientCamera(mc.timer.renderPartialTicks)
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val t = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        GL11.glLineWidth(1f)
        GlStateManager.color(var6, var7, var8, var11)
        val pos = API.getRenderUtil().entityRenderPos(to)
        val x = pos!![0]
        val y = pos[1] + to.eyeHeight
        val z = pos[2]
        val draw = 1
        t.startDrawing(draw)
        t.addVertex(0.0, mc.thePlayer.eyeHeight.toDouble(), 0.0)
        t.addVertex(x, y, z)
        var9.draw()
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    fun line(from: Vec3, to: Vec3, color: Int, width: Float) {
        GlStateManager.loadIdentity()
        mc.entityRenderer.orientCamera(mc.timer.renderPartialTicks)
        val var11 = (color shr 24 and 255).toFloat() / 255.0f
        val var6 = (color shr 16 and 255).toFloat() / 255.0f
        val var7 = (color shr 8 and 255).toFloat() / 255.0f
        val var8 = (color and 255).toFloat() / 255.0f
        val var9 = Tessellator.getInstance()
        val t = var9.worldRenderer
        GlStateManager.enableBlend()
        GlStateManager.disableTexture2D()
        GL11.glEnable(GL11.GL_LINE_SMOOTH)
        GL11.glLineWidth(width)
        GlStateManager.color(var6, var7, var8, var11)
        val mc = Minecraft.getMinecraft()
        val pf = API.getRenderUtil().renderPos(from)
        val pt = API.getRenderUtil().renderPos(to)
        val draw = 1
        t.startDrawing(draw)
        t.addVertex(pf[0], pf[1], pf[2])
        t.addVertex(pf[0], pt[1], pt[2])
        var9.draw()
        GL11.glDisable(GL11.GL_LINE_SMOOTH)
        GlStateManager.enableTexture2D()
        GlStateManager.disableBlend()
    }

    fun renderTrail(
        x: Double,
        y: Double,
        z: Double,
        mx: Double,
        my: Double,
        mz: Double,
        drag: Float,
        waterDrag: Float,
        grav: Float,
        color: Int,
        renderBlockHit: Boolean
    ) {
        var x = x
        var y = y
        var z = z
        var mx = mx
        var my = my
        var mz = mz
        for (j in 0..199) {
            val oldX = x
            val oldY = y
            val oldZ = z
            GlStateManager.enableDepth()
            GlStateManager.depthMask(true)
            line(Vec3(x, y, z), Vec3(x + mx, y + my, z + mz), color, 2f)
            GlStateManager.disableDepth()
            GlStateManager.depthMask(false)
            x += mx
            y += my
            z += mz
            val op = Vec3(oldX, oldY, oldZ)
            val np = Vec3(x, y, z)
            val mop = Minecraft.theWorld.rayTraceBlocks(op, np, false, true, true)
            if (mop != null) {
                if (mop.typeOfHit == MovingObjectType.BLOCK && renderBlockHit) {
                    val a = (color shr 24 and 255).toFloat() / 255.0f
                    blockBox(
                        mop.blockPos,
                        API.getUtil().reAlpha(color, a / 2),
                        API.getUtil().reAlpha(color, a),
                        booleanArrayOf(true, true, true, true, true, true)
                    )
                    break
                }
            }
            var var25 = drag
            val pos = Vector3d(x, y, z)
            val size = 0.25f
            if (Minecraft.theWorld.isAABBInMaterial(
                    AxisAlignedBB(
                        pos.x + size,
                        pos.y + size,
                        pos.z + size,
                        pos.x - size,
                        pos.y - size,
                        pos.z - size
                    ), Material.water
                )
            ) {
                if (waterDrag == 0f) {
                    break
                }
                var25 = waterDrag
            }
            mx *= var25.toDouble()
            my *= var25.toDouble()
            mz *= var25.toDouble()
            my -= grav.toDouble()
        }
    }

    fun cubeFill(
        draw: Int,
        minX: Double,
        minY: Double,
        minZ: Double,
        maxX: Double,
        maxY: Double,
        maxZ: Double,
        rotation: FloatArray,
        faces: BooleanArray
    ) {
        var minX = minX
        var minY = minY
        var minZ = minZ
        var maxX = maxX
        var maxY = maxY
        var maxZ = maxZ
        val var9 = Tessellator.getInstance()
        val t = var9.worldRenderer
        GlStateManager.pushMatrix()
        GlStateManager.translate(minX + (maxX - minX) / 2, minY, minZ + (maxZ - minZ) / 2)
        GlStateManager.rotate(rotation[0], 1f, 0f, 0f)
        GlStateManager.rotate(rotation[1], 0f, 1f, 0f)
        GlStateManager.rotate(rotation[2], 0f, 0f, 1f)
        maxX -= minX
        maxY -= minY
        maxZ -= minZ
        maxX /= 2.0
        maxZ /= 2.0
        minX = -maxX
        minY = 0.0
        minZ = -maxZ
        val array = arrayOf(
            arrayOf(
                doubleArrayOf(minX, minY, minZ),
                doubleArrayOf(minX, maxY, minZ),
                doubleArrayOf(maxX, maxY, minZ),
                doubleArrayOf(maxX, minY, minZ)
            ),
            arrayOf(
                doubleArrayOf(minX, minY, maxZ),
                doubleArrayOf(maxX, minY, maxZ),
                doubleArrayOf(maxX, maxY, maxZ),
                doubleArrayOf(minX, maxY, maxZ)
            ),
            arrayOf(
                doubleArrayOf(maxX, minY, minZ),
                doubleArrayOf(maxX, maxY, minZ),
                doubleArrayOf(maxX, maxY, maxZ),
                doubleArrayOf(maxX, minY, maxZ)
            ),
            arrayOf(
                doubleArrayOf(minX, minY, minZ),
                doubleArrayOf(minX, minY, maxZ),
                doubleArrayOf(minX, maxY, maxZ),
                doubleArrayOf(minX, maxY, minZ)
            ),
            arrayOf(
                doubleArrayOf(minX, maxY, minZ),
                doubleArrayOf(minX, maxY, maxZ),
                doubleArrayOf(maxX, maxY, maxZ),
                doubleArrayOf(maxX, maxY, minZ)
            ),
            arrayOf(
                doubleArrayOf(minX, minY, minZ),
                doubleArrayOf(maxX, minY, minZ),
                doubleArrayOf(maxX, minY, maxZ),
                doubleArrayOf(minX, minY, maxZ)
            )
        )
        t.startDrawing(draw)
        for (i in array.indices) {
            if (faces[i]) {
                for (j in array[i].indices) {
                    t.addVertex(array[i][j][0], array[i][j][1], array[i][j][2])
                }
                for (j in array[i].size - 1 downTo 0) {
                    t.addVertex(array[i][j][0], array[i][j][1], array[i][j][2])
                }
            }
        }
        var9.draw()
        GlStateManager.popMatrix()
    }

    fun cubeOutline(
        draw: Int,
        minX: Double,
        minY: Double,
        minZ: Double,
        maxX: Double,
        maxY: Double,
        maxZ: Double,
        rotation: FloatArray,
        faces: BooleanArray
    ) {
        var minX = minX
        var minY = minY
        var minZ = minZ
        var maxX = maxX
        var maxY = maxY
        var maxZ = maxZ
        val var9 = Tessellator.getInstance()
        val t = var9.worldRenderer
        GlStateManager.pushMatrix()
        GlStateManager.translate(minX + (maxX - minX) / 2, minY, minZ + (maxZ - minZ) / 2)
        GlStateManager.rotate(rotation[0], 1f, 0f, 0f)
        GlStateManager.rotate(rotation[1], 0f, 1f, 0f)
        GlStateManager.rotate(rotation[2], 0f, 0f, 1f)
        maxX -= minX
        maxY -= minY
        maxZ -= minZ
        maxX /= 2.0
        maxZ /= 2.0
        minX = -maxX
        minY = 0.0
        minZ = -maxZ

        //n s e w t b
        val vertices = booleanArrayOf(
            if (!faces[0] || !faces[2]) false else true,
            if (!faces[0] || !faces[3]) false else true,
            if (!faces[1] || !faces[2]) false else true,
            if (!faces[1] || !faces[3]) false else true,
            if (!faces[0] || !faces[5]) false else true,
            if (!faces[1] || !faces[5]) false else true,
            if (!faces[2] || !faces[5]) false else true,
            if (!faces[3] || !faces[5]) false else true,
            if (!faces[0] || !faces[4]) false else true,
            if (!faces[1] || !faces[4]) false else true,
            if (!faces[2] || !faces[4]) false else true,
            if (!faces[3] || !faces[4]) false else true
        )
        val array = arrayOf(
            arrayOf(doubleArrayOf(maxX, minY, minZ), doubleArrayOf(maxX, maxY, minZ)),
            arrayOf(doubleArrayOf(minX, minY, minZ), doubleArrayOf(minX, maxY, minZ)),
            arrayOf(doubleArrayOf(maxX, minY, maxZ), doubleArrayOf(maxX, maxY, maxZ)),
            arrayOf(doubleArrayOf(minX, minY, maxZ), doubleArrayOf(minX, maxY, maxZ)),
            arrayOf(doubleArrayOf(minX, minY, minZ), doubleArrayOf(maxX, minY, minZ)),
            arrayOf(doubleArrayOf(minX, minY, maxZ), doubleArrayOf(maxX, minY, maxZ)),
            arrayOf(doubleArrayOf(maxX, minY, minZ), doubleArrayOf(maxX, minY, maxZ)),
            arrayOf(doubleArrayOf(minX, minY, minZ), doubleArrayOf(minX, minY, maxZ)),
            arrayOf(doubleArrayOf(minX, maxY, minZ), doubleArrayOf(maxX, maxY, minZ)),
            arrayOf(doubleArrayOf(minX, maxY, maxZ), doubleArrayOf(maxX, maxY, maxZ)),
            arrayOf(doubleArrayOf(maxX, maxY, minZ), doubleArrayOf(maxX, maxY, maxZ)),
            arrayOf(doubleArrayOf(minX, maxY, minZ), doubleArrayOf(minX, maxY, maxZ))
        )
        t.startDrawing(draw)
        for (i in array.indices) {
            if (vertices[i]) {
                for (j in array[i].indices) {
                    t.addVertex(array[i][j][0], array[i][j][1], array[i][j][2])
                }
            }
        }
        var9.draw()
        GlStateManager.popMatrix()
    }

    fun render3DNameTag(
        text: String?,
        x: Double,
        y: Double,
        z: Double,
        wx: Double,
        wy: Double,
        wz: Double,
        active: Boolean
    ) {
        if (active) {
            val distance = Minecraft.getMinecraft().thePlayer.getDistance(wx, wy, wz).toFloat()
            val scaleFactor = if (distance < 10.0f) 0.9f else distance / 11.11f
            val color = 16777215
            var height = 0.0f
            if (distance >= 10.0f) {
                height = (height + (distance / 40.0f - 0.25)).toFloat()
            }
            val var12 = mc.fontRendererObj ?: return
            val var13 = 1.6f
            val var14 = 0.016666668f * var13
            GlStateManager.pushMatrix()
            GlStateManager.translate(x.toFloat() + 0.0f, y.toFloat() + 0.5f, z.toFloat())
            GL11.glNormal3f(0.0f, 1.0f, 0.0f)
            GlStateManager.rotate(-mc.renderManager.playerViewY, 0.0f, 1.0f, 0.0f)
            GlStateManager.rotate(mc.renderManager.playerViewX, 1.0f, 0.0f, 0.0f)
            GlStateManager.scale(-var14 * scaleFactor, -var14 * scaleFactor, var14 * scaleFactor)
            GlStateManager.disableLighting()
            GlStateManager.depthMask(false)
            GlStateManager.disableDepth()
            GlStateManager.enableBlend()
            GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
            val var15 = Tessellator.getInstance()
            val var16 = var15.worldRenderer
            GlStateManager.disableTexture2D()
            val var18 = var12.getStringWidth(text) / 2
            val w = var18.toFloat()
            val h = var12.FONT_HEIGHT.toFloat()
            val offY = 0f
            API.getDraw().rectBorder(-w - 4, -4 + offY, w + 4, h + 3 + offY, -0xfefeff)
            API.getDraw().rect(-w - 3, -3 + offY, w + 3, h + 2 + offY, -0x55fefeff)
            GlStateManager.enableTexture2D()
            val co = -1
            var12.drawString(text, -var12.getStringWidth(text) / 2, 0, co)
            GlStateManager.disableBlend()
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f)
            GlStateManager.popMatrix()
            return
        }
        val var12 = mc.fontRendererObj
        val var11 = text
        val var120 = 0.02666667f
        GlStateManager.alphaFunc(516, 0.1f)
        val distance = Minecraft.getMinecraft().thePlayer.getDistance(wx, wy, wz).toFloat()
        val scaleFactor = if (distance < 10.0f) 0.9f else distance / 11.11f
        val var13 = 1.6f
        val var14 = 0.016666668f * var13
        GlStateManager.pushMatrix()
        GlStateManager.translate(x.toFloat() + 0.0f, y.toFloat() + 0.5f, z.toFloat())
        GL11.glNormal3f(0.0f, 1.0f, 0.0f)
        GlStateManager.rotate(-mc.renderManager.playerViewY, 0.0f, 1.0f, 0.0f)
        GlStateManager.rotate(mc.renderManager.playerViewX, 1.0f, 0.0f, 0.0f)
        GlStateManager.scale(-var14 * scaleFactor, -var14 * scaleFactor, var14 * scaleFactor)
        GlStateManager.disableLighting()
        GlStateManager.depthMask(false)
        GlStateManager.disableDepth()
        GlStateManager.enableBlend()
        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0)
        val var15 = Tessellator.getInstance()
        val var16 = var15.worldRenderer
        val var17: Byte = 0
        GlStateManager.disableTexture2D()
        var16.startDrawingQuads()
        val var18 = var12.getStringWidth(text) / 2
        var16.setColorRGBA_F(0.0f, 0.0f, 0.0f, 0.25f)
        var16.addVertex((-var18 - 1).toDouble(), (-1 + var17).toDouble(), 0.0)
        var16.addVertex((-var18 - 1).toDouble(), (8 + var17).toDouble(), 0.0)
        var16.addVertex((var18 + 1).toDouble(), (8 + var17).toDouble(), 0.0)
        var16.addVertex((var18 + 1).toDouble(), (-1 + var17).toDouble(), 0.0)
        var15.draw()
        GlStateManager.enableTexture2D()
        var12.drawString(text, -var12.getStringWidth(text) / 2, var17.toInt(), 553648127)
        var12.drawString(text, -var12.getStringWidth(text) / 2, var17.toInt(), -1)
        GlStateManager.disableBlend()
        GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f)
        GlStateManager.popMatrix()
    }
}