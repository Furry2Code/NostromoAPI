package com.kilo.render;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.ByteArrayInputStream;

public class JTextureImage {

    public byte[] pixels;
    public Texture texture;
    public String location;

    public Texture getTexture() {
        if (texture == null) {
            if (pixels != null) {
                try {
                    ByteArrayInputStream bias = new ByteArrayInputStream(pixels);
                    texture = TextureLoader.getTexture("PNG", bias);
                } catch (Exception e) {
                }
            }
        }
        return texture;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
}
