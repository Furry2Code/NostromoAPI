package com.kilo.render

import org.newdawn.slick.opengl.Texture
import org.newdawn.slick.opengl.TextureLoader
import java.io.ByteArrayInputStream

class TextureImage {
    var pixels: ByteArray? = null
    var textures: Texture? = null
    var location: String? = null
    fun getTexture(): Texture? {
        if (textures == null) {
            if (pixels != null) {
                try {
                    val bias = ByteArrayInputStream(pixels)
                    textures = TextureLoader.getTexture("PNG", bias)
                } catch (e: Exception) {
                }
            }
        }
        return textures
    }
}