package com.kilo.render

import com.kilo.TrueTypeFont
import java.awt.Font

object Fonts {
    val instance: Fonts = this
    private const val standard = "roboto"
    private const val rounded = "comfortaa"
    val ttfStandard12 =
        loadTTF("$standard-bold.ttf", Font.PLAIN, 12f)
    val ttfStandard14 =
        loadTTF("$standard-regular.ttf", Font.PLAIN, 14f)
    val ttfStandard20 =
        loadTTF("$standard-regular.ttf", Font.PLAIN, 20f)
    val ttfStandard25 =
        loadTTF("$standard-regular.ttf", Font.PLAIN, 25f)
    val ttfRoundedBold10 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 10f)
    val ttfRoundedBold12 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 12f)
    val ttfRoundedBold14 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 14f)
    val ttfRoundedBold16 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 16f)
    val ttfRoundedBold20 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 20f)
    val ttfRoundedBold25 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 25f)
    val ttfRoundedBold40 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 40f)
    val ttfRoundedBold50 =
        loadTTF("$rounded-bold.ttf", Font.PLAIN, 50f)
    val ttfRounded10 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 10f)
    val ttfRounded12 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 12f)
    val ttfRounded14 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 14f)
    val ttfRounded16 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 16f)
    val ttfRounded20 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 20f)
    val ttfRounded25 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 25f)
    val ttfRounded40 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 40f)
    val ttfRounded50 =
        loadTTF("$rounded-regular.ttf", Font.PLAIN, 50f)

    fun loadTTF(path: String, style: Int, size: Float): TrueTypeFont {
        var path = path
        path = "/assets/kilo/fonts/$path"
        try {
            val inputStream = Fonts::class.java.getResourceAsStream(path)
            var font = Font.createFont(Font.TRUETYPE_FONT, inputStream)
            font = font.deriveFont(style, size)
            return TrueTypeFont(font, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return TrueTypeFont(Font("Tahoma", Font.PLAIN, size.toInt()), true)
    }
}