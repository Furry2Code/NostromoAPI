package com.kilo.sessions;

public enum EnumSessionType {
    GUEST(),
    USER(),
    ADMINISTRATOR()
}
