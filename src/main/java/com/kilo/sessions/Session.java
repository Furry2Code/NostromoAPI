package com.kilo.sessions;

import com.kilo.event.EventBus;
import com.kilo.sessions.event.*;
import com.kilo.util.CryptPass;

import java.util.UUID;

public class Session extends CryptPass {
    private UUID id;
    private String nickname;
    private String password;
    private EnumSessionType type;
    private String avatar;

    public Session(UUID id, String nickname, String password, EnumSessionType type, String avatar) {
        this.id = id;
        this.nickname = nickname;
        this.password = toSHA256(password);
        this.type = type;
        this.avatar = avatar;
    }

    public void onLogin() {
        LoginSessionEvent event = new LoginSessionEvent();
        EventBus.INSTANCE.post(event);
    }

    public void onVerfied() {
        VerifiedSessionEvent event = new VerifiedSessionEvent();
        EventBus.INSTANCE.post(event);
    }

    public void onLogOut() {
       LogOutSessionEvent event = new LogOutSessionEvent();
        EventBus.INSTANCE.post(event);
    }
    public void onUpdate() {
        UpdateSessionEvent event = new UpdateSessionEvent();
        EventBus.INSTANCE.post(event);
    }
    public void onLoadFolder() {
        LoadFolderSessionEvent event = new LoadFolderSessionEvent();
        EventBus.INSTANCE.post(event);
    }
    public void onCreateFolder() {
        CreateFolderSessionEvent event = new CreateFolderSessionEvent();
        EventBus.INSTANCE.post(event);
    }
    public void onAddFile() {
        AddFileSessionEvent event = new AddFileSessionEvent();
        EventBus.INSTANCE.post(event);
    }
    public void onRemoveFile() {
        RemoveFileSessionEvent event = new RemoveFileSessionEvent();
        EventBus.INSTANCE.post(event);
    }
    public void onLoadProfile() {
        LoadSessionProfileEvent event = new LoadSessionProfileEvent();
        EventBus.INSTANCE.post(event);
    }
    public void onNotify() {
        NotificationEvent event = new NotificationEvent();
        EventBus.INSTANCE.post(event);
    }

    public UUID getId() {
        return id;
    }

    public String getID() {
        return id.toString().replace("-","");
    }

    public String getNickname() {
        return nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public EnumSessionType getType() {
        return type;
    }

    public String getPassword() {
        return password;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setType(EnumSessionType type) {
        this.type = type;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
