package com.kilo.inputimport

import com.kilo.event.EventBus
import com.kilo.event.nostromo.*
import com.nostromo.api.input.IInput
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse
import org.lwjgl.opengl.Display

class Input : IInput {
    var mouse = IntArray(2)
    override fun handle() {
        if (Keyboard.isCreated()) {
            Keyboard.enableRepeatEvents(true)
            while (Keyboard.next()) {
                if (Keyboard.getEventKeyState()) {
                    val press = NKeyPressedEvent(Keyboard.getEventKey())
                    EventBus.INSTANCE.post(press)
                    val typed = NKeyTypedEvent(Keyboard.getEventKey(), Keyboard.getEventCharacter())
                    EventBus.INSTANCE.post(typed)
                } else {
                    val event = NKeyReleasedEvent(Keyboard.getEventKey())
                    EventBus.INSTANCE.post(event)
                }
            }
            Keyboard.enableRepeatEvents(false)
        }
        if (Mouse.isCreated()) {
            while (Mouse.next()) {
                if (Mouse.getEventButton() == -1) {
                    if (Mouse.getEventDWheel() != 0) {
                        val NMouseScroll = NMouseScrollEvent(Mouse.getEventDWheel())
                        EventBus.INSTANCE.post(NMouseScroll)
                    }
                    mouse[0] = Mouse.getEventX()
                    mouse[1] = Display.getHeight() - Mouse.getEventY()
                } else {
                    if (Mouse.getEventButtonState()) {
                        val NMouseClick = NMouseClickEvent(Mouse.getX(),Mouse.getY(), Display.getHeight(), Mouse.getEventButton())
                        EventBus.INSTANCE.post(NMouseClick)
                    } else {
                       val nMouse = NMouseReleasedEvent(Mouse.getX(), Mouse.getY(), Display.getHeight(), Mouse.getEventButton())
                        EventBus.INSTANCE.post(nMouse)
                    }
                }
            }
        }
    }
}