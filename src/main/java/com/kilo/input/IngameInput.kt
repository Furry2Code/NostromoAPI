package com.kilo.inputimport

import com.kilo.event.EventBus
import com.kilo.event.nostromo.NKeyPressedEvent
import com.kilo.event.nostromo.NKeyReleasedEvent
import com.nostromo.api.input.IIngameInput

open class IngameInput : IIngameInput{
    val instance: IngameInput
        get() {
            return instance
        }

    override fun keyboardPress(key: Int) {
        val pressedEvent = NKeyPressedEvent(key)
        EventBus.INSTANCE.post(pressedEvent)
    }

    override fun keyboardRelease(key: Int) {
        val event = NKeyReleasedEvent(key)
        EventBus.INSTANCE.post(event)
    }
}