package com.kilo.encryption;

import com.nostromo.api.encryption.ICipherFiles;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;


public class CipherFiles implements ICipherFiles {
    private static KeyGenerator keyGenerator;
    private static SecretKey key;
    private static final byte[] nook = new byte[16];

    @Override
    public String startEncode(String inputs) {
        try {
        keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        key = keyGenerator.generateKey();

        SecureRandom random = new SecureRandom();
        random.nextBytes(nook);
        byte[] input = encrypt(inputs.getBytes());
        return Base64.getEncoder().encodeToString(input);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "Error";
    }

    @Override
    public byte[] encrypt(byte[] input) {
        try {
        Cipher cipher  = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(nook);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        return cipher.doFinal(input);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return new byte[] {0x0010,0x0005};
    }

    @Override
    public String decrypt(byte[] input) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(nook);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] decryptedText = cipher.doFinal(input);
            return new String(decryptedText);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "You are fucking retard!";
    }
}
