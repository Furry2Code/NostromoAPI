package com.kilo.util;

import club.minnced.discord.rpc.DiscordEventHandlers;
import club.minnced.discord.rpc.DiscordRPC;
import club.minnced.discord.rpc.DiscordRichPresence;
import com.kilo.event.InvokeEvent;
import com.kilo.event.Priority;
import com.kilo.event.client.InitializationEvent;
import com.kilo.event.client.PreInitializationEvent;

public class DiscordRPCs {
    private final DiscordRPC lib = DiscordRPC.INSTANCE;
    private Thread thread;

    private void startRPC(DiscordRichPresence presence) {
        String applicationId = "738097989960204309";
        String steamId = "";
        DiscordEventHandlers handlers = new DiscordEventHandlers();
        handlers.ready = (user) -> System.out.println("Ready!");
        lib.Discord_Initialize(applicationId, handlers, true, steamId);
        lib.Discord_UpdatePresence(presence);

        thread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                lib.Discord_RunCallbacks();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ignored) {}
            }
        }, "RPC-Callback-Handler");
        thread.start();
        thread.setName("Discord RPC KiLO");
    }

    public void update(String[] details) {
        disconnect();
        DiscordRichPresence presence = new DiscordRichPresence();
        presence.details = details[0];
        presence.state = details[1];
        presence.smallImageKey = details[2];
        presence.largeImageKey = details[3];
        presence.smallImageText = details[4];
        presence.largeImageText = details[5];
        presence.startTimestamp = System.currentTimeMillis() / 1000;
        startRPC(presence);
    }

    private void disconnect() {
        lib.Discord_Shutdown();
    }

    @InvokeEvent(priority = Priority.HIGH)
    public void onInitialize(InitializationEvent event) {
        update(new String[] {"Initialization","Nostromo","nostromo-logged","nostromo-logo","Connected","Nostromo v4.0.15"});
    }

    @InvokeEvent
    public void onPost(PreInitializationEvent event) {
        update(new String[] {"Starting all things","Nostromo","nostromo-logged","nostromo-logo","Connected","Nostromo v4.0.15"});
    }
}
