package com.kilo.util;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ParticleJsLoader {

    public static void init() throws Exception {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");
        engine.eval(Files.newBufferedReader(Paths.get("assets/kilo/js/particle.js")));

        Invocable inv = (Invocable)engine;
        inv.invokeFunction("load","particles-js","assets/particles.json");
    }
}
