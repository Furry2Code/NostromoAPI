package com.kilo.utilimport

import com.kilo.API
import com.nostromo.api.utils.IRotationUtils
import net.minecraft.client.Minecraft
import net.minecraft.entity.EntityLivingBase
import net.minecraft.util.MathHelper

class RotationUtils : IRotationUtils {
    override fun getRotations(ent: EntityLivingBase): FloatArray {
        val x: Double = ent.posX
        val z: Double = ent.posZ
        val y: Double = ent.posY + ent.getEyeHeight() / 2.0f
        return getRotationFromPosition(x, z, y)
    }

    override fun getAverageRotations(targetList: List<EntityLivingBase>): FloatArray {
        var posX = 0.0
        var posY = 0.0
        var posZ = 0.0
        for (ent in targetList) {
            posX += ent.posX
            posY += ent.boundingBox.maxY - 2.0
            posZ += ent.posZ
        }
        posX /= targetList.size.toDouble()
        posY /= targetList.size.toDouble()
        posZ /= targetList.size.toDouble()
        return floatArrayOf(
            getRotationFromPosition(posX, posZ, posY)[0],
            getRotationFromPosition(posX, posZ, posY)[1]
        )
    }

    override fun getRotationFromPosition(x: Double, z: Double, y: Double): FloatArray {
        val xDiff: Double = x - API.clientUtils.mc().thePlayer!!.posX
        val zDiff: Double = z - API.clientUtils.mc().thePlayer!!.posZ
        val yDiff: Double = y - API.clientUtils.mc().thePlayer!!.posY - 0.6
        val dist: Double = MathHelper.sqrt_double(xDiff * xDiff + zDiff * zDiff).toDouble()
        val yaw = (Math.atan2(zDiff, xDiff) * 180.0 / 3.141592653589793).toFloat() - 90.0f
        val pitch = (-(Math.atan2(yDiff, dist) * 180.0 / 3.141592653589793)).toFloat()
        return floatArrayOf(yaw, pitch)
    }

    override fun getTrajAngleSolutionLow(d3: Float, d1: Float, velocity: Float): Float {
        val g = 0.006f
        val sqrt =
            velocity * velocity * velocity * velocity - g * (g * (d3 * d3) + 2.0f * d1 * (velocity * velocity))
        return Math.toDegrees(Math.atan((velocity * velocity - Math.sqrt(sqrt.toDouble())) / (g * d3))).toFloat()
    }

    override fun getNewAngle(angle: Float): Float {
        var angle = angle
        angle %= 360.0f
        if (angle >= 180.0f) {
            angle -= 360.0f
        }
        if (angle < -180.0f) {
            angle += 360.0f
        }
        return angle
    }

    override fun getDistanceBetweenAngles(angle1: Float, angle2: Float): Float {
        var angle3 = Math.abs(angle1 - angle2) % 360.0f
        if (angle3 > 180.0f) {
            angle3 = 360.0f - angle3
        }
        return angle3
    }
}