package com.kilo.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.kilo.API;
import com.kilo.managerimport.TextureManager;
import com.kilo.render.JTextureImage;
import com.nostromo.api.utils.IResources;
import net.minecraft.client.SplashScreen;
import net.minecraft.client.mod.ModManager;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.IOUtils;
import org.newdawn.slick.loading.DeferredResource;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.opengl.InternalTextureLoader;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import com.kilo.render.TextureImage;


public class Resources implements IResources {
    private static final int TOTAL_IMAGES = 200;
    private static final boolean FLAG_ASYNC_LOADING = false;
    private static final Object loadingLock = new Object();

    public static File soundNotification = new File("./KiLO/audio/notification.mp3");
    public static int ccs = 0;
    public static Texture backgroundLight;
    public static Texture backgroundDim;
    public static Texture branding;
    public static Texture brandingSmall;
    public static Texture createWorld;
    public static Texture wizard;

    public static ResourceLocation textureBlank = new ResourceLocation("textures/blank.png");
    public static File loc = new File("./KiLO/texture/users");

    public static Texture[] iconAccept = new Texture[6];
    public static Texture[] iconAdd = new Texture[6];
    public static Texture[] iconAdventure = new Texture[6];
    public static Texture[] iconArrowDown = new Texture[6];
    public static Texture[] iconArrowUp = new Texture[6];
    public static Texture[] iconBack = new Texture[6];
    public static Texture[] iconCharts = new Texture[6];
    public static Texture[] iconClose = new Texture[6];
    public static Texture[] iconCreative = new Texture[6];
    public static Texture[] iconDecline = new Texture[6];
    public static Texture[] iconDelete = new Texture[6];
    public static Texture[] iconEdit = new Texture[6];
    public static Texture[] iconExit = new Texture[6];
    public static Texture[] iconGoto = new Texture[6];
    public static Texture[] iconHardcore = new Texture[6];
    public static Texture[] iconHome = new Texture[6];
    public static Texture[] iconKey = new Texture[6];
    public static Texture[] iconMultiplayer = new Texture[6];
    public static Texture[] iconMusic = new Texture[6];
    public static Texture[] iconNext = new Texture[6];
    public static Texture[] iconPause = new Texture[6];
    public static Texture[] iconPlay = new Texture[6];
    public static Texture[] iconPlus = new Texture[6];
    public static Texture[] iconPrev = new Texture[6];
    public static Texture[] iconRefresh = new Texture[6];
    public static Texture[] iconReturn = new Texture[6];
    public static Texture[] iconSearch = new Texture[6];
    public static Texture[] iconSend = new Texture[6];
    public static Texture[] iconSettings = new Texture[6];
    public static Texture[] iconShield = new Texture[6];
    public static Texture[] iconSingleplayer = new Texture[6];
    public static Texture[] iconSpectator = new Texture[6];
    public static Texture[] iconStar = new Texture[6];
    public static Texture[] iconStatistics = new Texture[6];
    public static Texture[] iconSubmit = new Texture[6];
    public static Texture[] iconSurvival = new Texture[6];
    public static Texture[] iconTick = new Texture[6];
    public static Texture[] iconUser = new Texture[6];
    public static Texture[][] iconVolume = new Texture[3][6];
    public static Texture[] iconWifi = new Texture[6];
    public static Texture[] usrlogo = new Texture[10];

    public static Texture[] background = new Texture[TOTAL_IMAGES];

    public static ArrayList<Texture> userTexture = new ArrayList<Texture>();

    public static Texture iconSeparator;
    public static Texture[] iconHacks = new Texture[7];

    private static final String dom = "assets/kilo/";
    private static final String tex = dom+"textures/";
    private static final String fnt = dom+"fonts/";

    private static final String anim = "assets/anim/";

    @Override
    public void loadWithThread() {
        if (FLAG_ASYNC_LOADING) {
            synchronized (loadingLock) {
                new Thread(() -> {
                    try {
                        loadTextures();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }, "Async KiLO Resource Loader").start();
            }
        }
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public InputStream _load(String name) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        IOUtils.copy(ModManager.getClassLoader().getResourceAsStream(name), baos);
        return new ByteArrayInputStream(baos.toByteArray());
    }

    @Override
    public InputStream _load(InputStream stream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        IOUtils.copy(stream, baos);
        return new ByteArrayInputStream(baos.toByteArray());
    }

    @Override
    public void loadTextures() throws Exception {
        synchronized (loadingLock) {
            if (FLAG_ASYNC_LOADING) {
                InternalTextureLoader.get().setDeferredLoading(true);
            }
            backgroundLight = TextureLoader.getTexture("PNG", _load(tex + "gui/background-light.png"));
            backgroundDim = TextureLoader.getTexture("PNG", _load(tex + "gui/background-dim.png"));
            branding = TextureLoader.getTexture("PNG", _load(tex + "gui/branding.png"));
            brandingSmall = TextureLoader.getTexture("PNG", _load(tex + "gui/branding-small.png"));
            createWorld = TextureLoader.getTexture("PNG", _load(tex + "gui/create-world.png"));
            wizard = TextureLoader.getTexture("PNG", _load(tex + "gui/wizard.png"));

            iconSeparator = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/separator.png"));
            iconHacks[0] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/all.png"));
            iconHacks[1] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/build.png"));
            iconHacks[2] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/combat.png"));
            iconHacks[3] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/display.png"));
            iconHacks[4] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/misc.png"));
            iconHacks[5] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/movement.png"));
            iconHacks[6] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/hacks/player.png"));

            int[] sizes = new int[]{8, 16, 24, 32, 48, 64};
            for (int i = 0; i < 6; i++) {
                int j = sizes[i];
                iconAccept[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/accept.png"));
                iconAdd[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/add.png"));
                iconAdventure[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/adventure.png"));
                iconArrowDown[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/arrow_down.png"));
                iconArrowUp[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/arrow_up.png"));
                iconBack[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/back.png"));
                iconCharts[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/charts.png"));
                iconClose[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/close.png"));
                iconCreative[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/creative.png"));
                iconDecline[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/decline.png"));
                iconDelete[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/delete.png"));
                iconEdit[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/edit.png"));
                iconExit[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/exit.png"));
                iconGoto[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/goto.png"));
                iconHardcore[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/hardcore.png"));
                iconHome[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/home.png"));
                iconKey[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/key.png"));
                iconMultiplayer[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/multiplayer.png"));
                iconMusic[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/music.png"));
                iconNext[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/next.png"));
                iconPause[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/pause.png"));
                iconPlay[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/play.png"));
                iconPlus[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/plus.png"));
                iconPrev[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/prev.png"));
                iconRefresh[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/refresh.png"));
                iconReturn[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/return.png"));
                iconSearch[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/search.png"));
                iconSend[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/send.png"));
                iconSettings[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/settings.png"));
                iconShield[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/shield.png"));
                iconSingleplayer[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/singleplayer.png"));
                iconSpectator[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/spectate.png"));
                iconStar[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/star.png"));
                iconStatistics[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/statistics.png"));
                iconSubmit[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/submit.png"));
                iconSurvival[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/survival.png"));
                iconTick[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/tick.png"));
                iconUser[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/user.png"));
                iconVolume[0][i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/volume.png"));
                iconVolume[1][i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/volume1.png"));
                iconVolume[2][i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/volume2.png"));
                iconWifi[i] = TextureLoader.getTexture("PNG", _load(tex + "gui/icons/" + String.valueOf(j) + "/wifi.png"));
            }

            if (FLAG_ASYNC_LOADING) {
                if (!mainWaiting) loadBackground(false);
            } else {
                loadBackground(true);
            }

            if (FLAG_ASYNC_LOADING) {
                InternalTextureLoader.get().setDeferredLoading(false);
            }
        }
    }

    private static int c = 0;
    private static boolean mainWaiting = false;

    @Override
    public void loadBackground(boolean main) throws Exception {
        for (c = 0; c < TOTAL_IMAGES; c++) {
            background[c] = TextureLoader.getTexture("PNG", _load(anim + c + ".png"));
            if (main) {
                SplashScreen.setProgress(11, "Loading background " + c + "/" + TOTAL_IMAGES);
                SplashScreen.update();
            } else if (mainWaiting) {
                return;
            }
        }
    }

    @Override
    public void bindTextures() throws Exception {
        mainWaiting = true;
        SplashScreen.setProgress(10, "Loading resources...");
        SplashScreen.update();
        if (FLAG_ASYNC_LOADING) {
            synchronized (loadingLock) {
                LoadingList loadingList = LoadingList.get();
                DeferredResource deferredResource;
                while ((deferredResource = loadingList.getNext()) != null) {
                    deferredResource.load();
                }
                if (c != TOTAL_IMAGES) {
                    loadBackground(true);
                }
            }
        } else {
            loadTextures();
        }
    }

    @Override
    public void loadUserTexture() {}

    @Override
    public TextureImage downloadTexture(final String imageURL) {
        if (API.getTextureManager().exists(imageURL)) {
            if (API.getTextureManager().get(imageURL) != null) {
                return API.getTextureManager().get(imageURL);
            }
        }
        TextureImage textureImage = new TextureImage();
        textureImage.setLocation(imageURL);
        final TextureImage ti = textureImage;
        API.getTextureManager().getCaches().add(ti);
        new Thread() {
            @Override
            public void run() {
                try {
                    URL url = new URL(imageURL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0");
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    byte[] buf = new byte[1024];
                    int n = 0;
                    while (-1!=(n=in.read(buf)))
                    {
                        out.write(buf, 0, n);
                    }
                    out.close();
                    in.close();
                    conn.disconnect();
                    ti.setPixels(out.toByteArray());
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        }.start();
        return ti;
    }

    @Override
    public JTextureImage jDownloadTexture(int index, final String imageURL) {
        if (API.ijTextureManager.exists(imageURL)) {
            if (API.ijTextureManager.get(imageURL) != null) {
                return API.ijTextureManager.get(imageURL);
            }
        }
        JTextureImage textureImage = new JTextureImage();
        textureImage.setLocation(imageURL);
        final JTextureImage ti = textureImage;
        JTextureManager.cache.add(ti);
        new Thread() {
            @Override
            public void run() {
                try {
                    URL url = new URL(imageURL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0");
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    byte[] buf = new byte[1024];
                    int n = 0;
                    while (-1!=(n=in.read(buf)))
                    {
                        out.write(buf, 0, n);
                    }
                    out.close();
                    in.close();
                    ti.setPixels(out.toByteArray());
                    usrlogo[index] = ti.getTexture();
                    conn.disconnect();
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        }.start();
        return ti;
    }

    @Override
    public void onTickChange() {
        ccs++;
    }
}
