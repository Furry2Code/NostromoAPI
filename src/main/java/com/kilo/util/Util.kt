package com.kilo.util

import com.kilo.API
import com.kilo.utilimport.ClientUtils
import com.nostromo.api.utils.IUtil
import net.minecraft.client.Minecraft
import net.minecraft.entity.EntityLivingBase
import net.minecraft.util.BlockPos
import net.minecraft.util.Vec3
import org.json.JSONArray
import org.json.JSONObject
import java.awt.Color
import java.awt.Desktop
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URI
import java.net.URL
import java.util.*

class Util : IUtil{
    override fun isASCII(c: Char): Boolean {
        return "\u00c0\u00c1\u00c2\u00c8\u00ca\u00cb\u00cd\u00d3\u00d4\u00d5\u00da\u00df\u00e3\u00f5\u011f\u0130\u0131\u0152\u0153\u015e\u015f\u0174\u0175\u017e\u0207\u0000\u0000\u0000\u0000\u0000\u0000\u0000 !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u00c7\u00fc\u00e9\u00e2\u00e4\u00e0\u00e5\u00e7\u00ea\u00eb\u00e8\u00ef\u00ee\u00ec\u00c4\u00c5\u00c9\u00e6\u00c6\u00f4\u00f6\u00f2\u00fb\u00f9\u00ff\u00d6\u00dc\u00f8\u00a3\u00d8\u00d7\u0192\u00e1\u00ed\u00f3\u00fa\u00f1\u00d1\u00aa\u00ba\u00bf\u00ae\u00ac\u00bd\u00bc\u00a1\u00ab\u00bb\u2591\u2592\u2593\u2502\u2524\u2561\u2562\u2556\u2555\u2563\u2551\u2557\u255d\u255c\u255b\u2510\u2514\u2534\u252c\u251c\u2500\u253c\u255e\u255f\u255a\u2554\u2569\u2566\u2560\u2550\u256c\u2567\u2568\u2564\u2565\u2559\u2558\u2552\u2553\u256b\u256a\u2518\u250c\u2588\u2584\u258c\u2590\u2580\u03b1\u03b2\u0393\u03c0\u03a3\u03c3\u03bc\u03c4\u03a6\u0398\u03a9\u03b4\u221e\u2205\u2208\u2229\u2261\u00b1\u2265\u2264\u2320\u2321\u00f7\u2248\u00b0\u2219\u00b7\u221a\u207f\u00b2\u25a0\u0000".indexOf(
            c
        ) != -1
    }

    override fun safeDiv(x: Double, y: Double, z: Double): Double {
        return try {
            x / y
        } catch (e: Exception) {
            1
        } as Double
    }

    private val mc = API.getClientUtils().mc()
        val doubleClickTimer = 0.3f
    override fun reAlpha(color: Int, alpha: Float): Int {
            val c = Color(color)
            val r = 1.toFloat() / 255 * c.red
            val g = 1.toFloat() / 255 * c.green
            val b = 1.toFloat() / 255 * c.blue
            return Color(r, g, b, alpha).rgb
        }

    override fun blendColor(color1: Int, color2: Int, perc: Float): Int {
            val x = Color(color1)
            val y = Color(color2)
            val inverse_blending = 1 - perc
            val red = x.red * perc + y.red * inverse_blending
            val green = x.green * perc + y.green * inverse_blending
            val blue = x.blue * perc + y.blue * inverse_blending
            val blended: Color
            blended = try {
                Color(red / 255, green / 255, blue / 255)
            } catch (e: Exception) {
                Color(-1)
            }
            return blended.rgb
        }

    override  fun replaceFormat(s: String): String {
            return s.replace("(?i)&([a-f0-9])".toRegex(), "\u00a7$1")
        }

    override fun openWeb(s: String?) {
            try {
                val desktop = if (Desktop.isDesktopSupported()) Desktop.getDesktop() else null
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(URI(s))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            } catch (e: Exception) {
            }
        }

    override fun usernameHistory(username: String): List<String?> {
            var uuid: String? = null
            var names: List<String?> = ArrayList()
            try {
                uuid = getUUIDFromName(username)
            } catch (e: Exception) {
            }
            if (uuid == null) {
                return names
            }
            try {
                names = getNamesFromUUID(uuid)
            } catch (e: Exception) {
            }
            return names
        }

        override fun getUUIDFromName(username: String): String {
            val getUUID = URL("https://api.mojang.com/users/profiles/minecraft/$username")
            val `in` = BufferedReader(InputStreamReader(getUUID.openStream()))
            var inputLine: String?
            var line: String? = ""
            while (`in`.readLine().also { inputLine = it } != null) {
                line = inputLine
            }
            `in`.close()
            val jo = JSONObject(line)
            return jo.getString("id")
        }

    override fun getNamesFromUUID(uuid: String): List<String?> {
            val names: MutableList<String?> = ArrayList()
            val getNames = URL("https://api.mojang.com/user/profiles/$uuid/names")
            val `in` = BufferedReader(InputStreamReader(getNames.openStream()))
            var inputLine: String? = ""
            var line: String? = ""
            while (`in`.readLine().also { inputLine = it } != null) {
                line = inputLine
            }
            `in`.close()
            val ja = JSONArray(line)
            for (i in 0 until ja.length()) {
                val jao = JSONObject(ja[i].toString())
                names.add(jao.getString("name"))
            }
            return names
        }


    override fun prettyFloat(f: Double): String {
            return String.format("%s", f)
        }

    override  fun angleDifference(a: Float, b: Float): Float {
            return ((a - b) % 360 + 540f) % 360 - 180f
        }

    override fun makeFloat(o: Any): Float {
            return try {
                o.toString().toFloat()
            } catch (e: Exception) {
                (-1).toFloat()
            }
        }

    override  fun makeInteger(o: Any): Int {
            return try {
                prettyFloat(makeFloat(o).toDouble()).toInt()
            } catch (e: Exception) {
                -1
            }
        }

    override fun makeBoolean(o: Any): Boolean {
            return try {
                java.lang.Boolean.parseBoolean(o.toString())
            } catch (e: Exception) {
                false
            }
        }

    override fun canSeeEntity(from: EntityLivingBase, to: EntityLivingBase): Boolean {
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ), Vec3(to.posX, to.posY + to.eyeHeight.toDouble(), to.posZ)
                ) == null
            ) {
                return true
            }
            if (to.entityBoundingBox == null) {
                return false
            }
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.minX, to.entityBoundingBox.maxY, to.entityBoundingBox.minZ)
                ) == null
            ) {
                return true
            }
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.minX, to.entityBoundingBox.maxY, to.entityBoundingBox.maxZ)
                ) == null
            ) {
                return true
            }
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.maxX, to.entityBoundingBox.maxY, to.entityBoundingBox.maxZ)
                ) == null
            ) {
                return true
            }
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.maxX, to.entityBoundingBox.maxY, to.entityBoundingBox.minZ)
                ) == null
            ) {
                return true
            }
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.minX, to.entityBoundingBox.minY, to.entityBoundingBox.minZ)
                ) == null
            ) {
                return true
            }
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.minX, to.entityBoundingBox.minY, to.entityBoundingBox.maxZ)
                ) == null
            ) {
                return true
            }
            if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.maxX, to.entityBoundingBox.minY, to.entityBoundingBox.maxZ)
                ) == null
            ) {
                return true
            }
            return if (from.worldObj.rayTraceBlocks(
                    Vec3(
                        from.posX,
                        from.posY + from.eyeHeight.toDouble(),
                        from.posZ
                    ),
                    Vec3(to.entityBoundingBox.maxX, to.entityBoundingBox.minY, to.entityBoundingBox.minZ)
                ) == null
            ) {
                true
            } else false
        }

    override fun getRotationToBlockPos(bp: BlockPos): FloatArray {
            val pX = mc.thePlayer!!.posX
            val pY =
                mc.thePlayer!!.posY + mc.thePlayer!!.eyeHeight
            val pZ = mc.thePlayer!!.posZ
            val eX = bp.x + 0.5f.toDouble()
            val eY = bp.y + 0.5f.toDouble()
            val eZ = bp.z + 0.5f.toDouble()
            val dX = pX - eX
            val dY = pY - eY
            val dZ = pZ - eZ
            val dH = Math.sqrt(Math.pow(dX, 2.0) + Math.pow(dZ, 2.0))
            var yaw = 0f
            var pitch = 0f
            yaw = (Math.toDegrees(Math.atan2(dZ, dX)) + 90).toFloat()
            pitch = Math.toDegrees(Math.atan2(dH, dY)).toFloat()
            return floatArrayOf(yaw, 90 - pitch)
        }

    override fun getRotationToPos(x: Double, y: Double, z: Double): FloatArray {
            val pX = mc.thePlayer!!.posX
            val pY =
                mc.thePlayer!!.posY + mc.thePlayer!!.eyeHeight
            val pZ = mc.thePlayer!!.posZ
            val dX = pX - x
            val dY = pY - y
            val dZ = pZ - z
            val dH = Math.sqrt(Math.pow(dX, 2.0) + Math.pow(dZ, 2.0))
            var yaw = 0f
            var pitch = 0f
            yaw = (Math.toDegrees(Math.atan2(dZ, dX)) + 90).toFloat()
            pitch = Math.toDegrees(Math.atan2(dH, dY)).toFloat()
            return floatArrayOf(yaw, 90 - pitch)
        }
}