package com.kilo.utilimport

enum class ActivityType {
    new_message, friend_accepted, friend_request, invite
}