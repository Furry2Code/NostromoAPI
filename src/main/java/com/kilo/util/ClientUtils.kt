package com.kilo.utilimport

import com.nostromo.api.utils.IClientUtils
import net.minecraft.client.Minecraft
import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.client.multiplayer.WorldClient

class ClientUtils : IClientUtils {
    //this was skidded in like 3 mins for alt manager
    var mc: Minecraft = Minecraft.getMinecraft()
    override fun mc(): Minecraft {
        return Minecraft.getMinecraft()
    }

    override fun player(): EntityPlayerSP {
        return mc().thePlayer
    }

    override fun world(): WorldClient {
        mc()
        return Minecraft.theWorld
    }
}