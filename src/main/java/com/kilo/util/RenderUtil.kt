package com.kilo.util

import com.kilo.API
import com.nostromo.api.utils.IRenderUtil
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.entity.Entity
import net.minecraft.util.Vec3
import java.awt.Color

class RenderUtil : IRenderUtil {
    private val mc = API.clientUtils.mc()
    override fun entityRenderPos(e: Entity): DoubleArray {
        val p_147936_2_ = mc.timer.renderPartialTicks
        val x =
            e.lastTickPosX + (e.posX - e.lastTickPosX) * p_147936_2_.toDouble() - mc.renderManager!!.viewerPosX
        val y =
            e.lastTickPosY + (e.posY - e.lastTickPosY) * p_147936_2_.toDouble() - mc.renderManager!!.viewerPosY
        val z =
            e.lastTickPosZ + (e.posZ - e.lastTickPosZ) * p_147936_2_.toDouble() - mc.renderManager!!.viewerPosZ
        return doubleArrayOf(x, y, z)
    }

    override fun entityWorldPos(e: Entity): DoubleArray {
        val p_147936_2_ = mc.timer.renderPartialTicks
        val x = e.lastTickPosX + (e.posX - e.lastTickPosX) * p_147936_2_.toDouble()
        val y = e.lastTickPosY + (e.posY - e.lastTickPosY) * p_147936_2_.toDouble()
        val z = e.lastTickPosZ + (e.posZ - e.lastTickPosZ) * p_147936_2_.toDouble()
        return doubleArrayOf(x, y, z)
    }

    override  fun renderPos(vec: Vec3): DoubleArray {
        val p_147936_2_ = mc.timer.renderPartialTicks
        val x = vec.xCoord - mc.renderManager!!.viewerPosX
        val y = vec.yCoord - mc.renderManager!!.viewerPosY
        val z = vec.zCoord - mc.renderManager!!.viewerPosZ
        return doubleArrayOf(x, y, z)
    }

    override fun transparentEntity(id: Int): Boolean {
        return id == -1 || id == -2
    }

    override fun renderFar(): Boolean {
       return false
    }

    override fun hsvToRgb(hue: Float, saturation: Float, value: Float): String {
        val h = (hue * 6).toInt()
        val f = hue * 6 - h
        val p = value * (1 - saturation)
        val q = value * (1 - f * saturation)
        val t = value * (1 - (1 - f) * saturation)
        return when (h) {
            0 -> rgbToString(value, t, p)
            1 -> rgbToString(q, value, p)
            2 -> rgbToString(p, value, t)
            3 -> rgbToString(p, q, value)
            4 -> rgbToString(t, p, value)
            5 -> rgbToString(value, p, q)
            else -> throw RuntimeException("Something went Fnurf wrong when converting from HSV to RGB. Input was $hue, $saturation, $value")
        }
    }

    override fun rgbToString(r: Float, g: Float, b: Float): String {
        val rs = Integer.toHexString((r * 256).toInt())
        val gs = Integer.toHexString((g * 256).toInt())
        val bs = Integer.toHexString((b * 256).toInt())
        return rs + gs + bs
    }

    override  fun getRainbow(fade: Float): Color {
        val hue = (System.nanoTime() / 5000000000f) % 1
        val color =
            Integer.toHexString(Integer.valueOf(Color.HSBtoRGB(hue, 1f, 1f))).toLong(16)
        val c = Color(color.toInt())
        return Color(
            c.red / 255f * fade,
            c.green / 255f * fade,
            c.blue / 255f * fade,
            c.alpha / 255f
        )
    }

    override  fun setupFarRender() {
        mc.entityRenderer!!.setupCameraTransformExt(mc.timer.renderPartialTicks, 2)
    }

    override fun enableLighting() {
        GlStateManager.enableLight(0)
        GlStateManager.enableLight(1)
    }

    override  fun disableLighting() {
        GlStateManager.disableLight(0)
        GlStateManager.disableLight(1)
    }
}