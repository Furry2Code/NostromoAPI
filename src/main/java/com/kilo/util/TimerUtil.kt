package com.kilo.utilimport

import com.nostromo.api.utils.ITimerUtil
import java.util.concurrent.TimeUnit

class TimerUtil : ITimerUtil{
    val previousTime: Long = 0
    private var currentMS = 0L
    protected var lastMS = -1L

    private var prevMS = 0L
    override fun delay(milliSec: Float): Boolean {
        return time - prevMS >= milliSec
    }

    override fun update() {
        currentMS = System.currentTimeMillis()
    }

    override fun isDelayComplete(delay: Long): Boolean {
        return System.currentTimeMillis() - lastMS >= delay
    }

    override fun systemTime(): Long {
        return System.currentTimeMillis()
    }

    override fun currentTime(): Float {
        return (systemTime() - previousTime).toFloat()
    }

    override fun isTime(time: Float): Boolean {
        return currentTime() >= time * 1000L
    }

    fun setLastMS() {
        lastMS = System.currentTimeMillis()
    }

    override  fun reset() {
        prevMS = time
    }

    val time: Long
        get() = System.nanoTime() / 1000000L

    override  fun hasPassed(MS: Long): Boolean {
        return currentMS >= lastMS + MS
    }

    val difference: Long
        get() = time - prevMS

    override  fun hasReach(a: Long, milliseconds: TimeUnit?): Boolean {
        return hasReach(a, TimeUnit.MILLISECONDS)
    }

}