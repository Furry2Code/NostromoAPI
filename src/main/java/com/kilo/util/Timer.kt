package com.kilo.utilimport

import com.nostromo.api.utils.ITimer
import java.util.concurrent.TimeUnit

class Timer : ITimer{
    private var previousTime: Long = 0
    private val prevMS: Long = 0
   override fun hasReach(time: Long, timeUnit: TimeUnit): Boolean {
        val convert =
            timeUnit.convert(System.nanoTime() - previousTime, TimeUnit.NANOSECONDS)
        if (convert >= time) {
            reset()
        }
        return convert >= time
    }

    override fun isTime(time: Float): Boolean {
        return currentTime() >= time * 1000L
    }

    override fun currentTime(): Float {
        return (systemTime() - previousTime).toFloat()
    }

    override fun reset() {
        previousTime = systemTime()
    }

    override fun systemTime(): Long {
        return System.currentTimeMillis()
    }

    val time: Long
        get() = System.nanoTime() / 1000000L

    override fun delay(milliSec: Float): Boolean {
        return time - prevMS >= milliSec
    }

}