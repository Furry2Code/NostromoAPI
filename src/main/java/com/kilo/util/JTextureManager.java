package com.kilo.util;

import com.kilo.render.JTextureImage;
import com.kilo.render.TextureImage;
import com.nostromo.api.utils.IJTextureManager;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class JTextureManager implements IJTextureManager {


    public static List<JTextureImage> cache = new CopyOnWriteArrayList<JTextureImage>();

    @Override
    public boolean exists(String location) {
        return get(location) != null;
    }

    @Override
    public JTextureImage get(String location) {
        for(JTextureImage ti : cache) {
            if (ti.location.equalsIgnoreCase(location)) {
                return ti;
            }
        }
        return null;
    }
}
