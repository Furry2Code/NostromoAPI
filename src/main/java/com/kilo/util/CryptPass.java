package com.kilo.util;

import com.nostromo.api.utils.ICryptPass;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptPass implements ICryptPass
{

    protected static final String salt = "4356TYUHJGNBV5RTJGHN547E567575bvnxdfgbvc4356TY54g";

    @Override
    public String toSHA1(String text)
    {
        return crypt(text, "SHA-1");
    }

    @Override
    public String toSHA256(String text)
    {
        return crypt(salt + text, "SHA-256");
    }

    @Override
    public String toMD5(String text)
    {
        return crypt(text, "MD5");
    }

    @Override
    public String crypt(String text, String type)
    {
        String output = "";

        try
        {
            MessageDigest digest = MessageDigest.getInstance(type);
            digest.update(text.getBytes("UTF-8"));

            byte[] hash = digest.digest();
            BigInteger bigInt = new BigInteger(1, hash);
            output = bigInt.toString(16);

            while (output.length() < 32)
            {
                output = "0" + output;
            }
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException ignored) {}

        return output;
    }

    @Override
    public boolean compareHash(String text, String hash) {
        String text_hashed = toSHA256(salt + text);
        if (text_hashed.matches(hash)) {
            return true;
        } else {
            return false;
        }
    }
}