package com.kilo.utilimport

import com.kilo.API
import com.nostromo.api.utils.IFileUtils
import java.io.*
import java.util.*

class FileUtils : IFileUtils {
   override fun read(inputFile: File?): List<String> {
        val readContent: MutableList<String> = ArrayList()
        try {
            val `in` = BufferedReader(InputStreamReader(FileInputStream(inputFile), "UTF8"))
            var str: String
            while (`in`.readLine().also({ str = it }) != null) {
                //readContent.add(str)
            }
            `in`.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return readContent
    }

    override fun write(
        outputFile: File?,
        writeContent: List<String>,
        overrideContent: Boolean
    ) {
        try {
            val out: Writer = BufferedWriter(OutputStreamWriter(FileOutputStream(outputFile), "UTF-8"))
            for (outputLine in writeContent) {
                out.write(outputLine + System.getProperty("line.separator"))
            }
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val configDir: File
        get() {
            val file = File(API.clientUtils.mc().mcDataDir, "Waifu-Config")
            if (!file.exists()) {
                file.mkdir()
            }
            return file
        }

    override fun getConfigFile(name: String?): File {
        val file =
            File(this.configDir, String.format("%s.txt", name))
        if (!file.exists()) {
            try {
                file.createNewFile()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return file
    }
}