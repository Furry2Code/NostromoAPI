package com.kilo.utilimport

enum class Align {
    L, T, R, B, C
}