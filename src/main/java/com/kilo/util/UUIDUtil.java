package com.kilo.util;

import com.mojang.authlib.GameProfile;
import com.nostromo.api.utils.IUUIDUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;

import java.util.UUID;

/**
 * Created by mitchellkatz on 5/2/18. Designed for production use on Sk1er.club
 */
public class UUIDUtil implements IUUIDUtil {
    @Override
    public String getUUIDWithoutDashes() {
        return getClientUUID().toString().toLowerCase().replace("-", "");
    }

    @Override
    public UUID getClientUUID() {
        GameProfile profile = Minecraft.getMinecraft().getSession().getProfile();
        if (profile != null) {
            UUID id = profile.getId();
            if (id != null) return id;
        }

        EntityPlayerSP thePlayer = Minecraft.getMinecraft().thePlayer;
        if (thePlayer != null) return thePlayer.getUniqueID();
        return null;
    }
}