package com.kilo.util

import com.kilo.API
import com.kilo.renderimport.Format
import com.kilo.utilimport.ClientUtils
import com.kilo.utilimport.ColorUtil
import net.minecraft.util.ChatComponentText
import net.minecraft.util.ChatStyle
import net.minecraft.util.EnumChatFormatting
import net.minecraft.util.IChatComponent
import org.newdawn.slick.Color
import com.kilo.TrueTypeFont
import com.nostromo.api.utils.IChatUtil
import com.nostromo.api.utils.IColorUtil.colors
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class ChatUtil : IChatUtil {
    val instance: ChatUtil
        get () {
            TODO()
        }
    private var pattern: Pattern? = null
    private var matcher: Matcher? = null
    private val EMAIL_PATTERN =
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,3})$"

    override fun isAllowedCharacter(character: Char): Boolean {
        return character.toInt() != 167 && character.toInt() >= 32 && character.toInt() != 127
    }

    override fun isValidUsername(line: String?): Boolean {
        for (c in line!!.toCharArray()) {
            if (!Character.isLetterOrDigit(c) && c != '_') {
                return false
            }
        }
        return true
    }

    override fun send(message: String) {
        var message = message
        if (API.getClientUtils().world() != null && API.getClientUtils().player() != null) {
            message = "[v][[t]KiLO[v]][t]: $message"
            API.getClientUtils().player().addChatMessage(makeComponent(message))
        }
    }

    override  fun addFormat(message: String, regex: String): String {
        return message.replace("(?i)" + regex + "([0-9a-fklmnor])".toRegex(), "�$1")
    }

    override fun makeComponent(message: String): IChatComponent {
        var message = message
        message = message.replace("\t".toRegex(), "    ")
        for (key in API.getColorUtil().colors?.keys!!) {
            val c = API.getColorUtil().colors?.get(key)
            message = message.replace(c!!.regex, "&" + c.color)
        }
        message = addFormat(message, "&")
        val parts = message.split("§").toTypedArray()
        val icc: IChatComponent = ChatComponentText("")
        var array: Array<String>
        val length = parts.also { array = it }.size
        var j = 0
        while (j < length) {
            var part = array[j]
            if (part.isNotEmpty()) {
                val c2 = part[0]
                part = part.substring(1)
                val style = ChatStyle()
                when (c2) {
                    'k' -> {
                        style.setObfuscated(true)
                    }
                    'l' -> {
                        style.setBold(true)
                    }
                    'm' -> {
                        style.setUnderlined(true)
                    }
                    'n' -> {
                        style.setStrikethrough(true)
                    }
                    'o' -> {
                        style.setItalic(true)
                    }
                    else -> {
                        style.color = charToFormat(c2)
                    }
                }
                val lines = part.split("\n").toTypedArray()
                for (i in lines.indices) {
                    val line = lines[i]
                    icc.appendSibling(ChatComponentText(line).setChatStyle(style))
                    if (i != lines.size - 1) {
                        icc.appendSibling(ChatComponentText("\n"))
                    }
                }
            }
            ++j
        }
        return icc
    }

    override  fun charToFormat(c: Char): EnumChatFormatting {
        var values: Array<EnumChatFormatting?>
       // val length = EnumChatFormatting.values().size.also { values = it }.size
        var i = 0
       /*
        while (length > i) {
            val ecf = values[i]
            ++i
        }
        */
        return EnumChatFormatting.RESET
    }

    override fun isValidEmail(line: String?): Boolean {
        for (c in line!!.toCharArray()) {
            if (!Character.isLetterOrDigit(c) && c != '_' && c != '@' && c != '.') {
                return false
            }
        }
        return validateEmail(line)
    }

    override fun validateEmail(hex: String?): Boolean {
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern!!.matcher(hex)
        return matcher!!.matches()
    }

    override fun isNumber(line: String?): Boolean {
        return try {
            line!!.toInt()
            true
        } catch (e: Exception) {
            false
        }
    }

    override fun insertAt(line: String?, character: String, index: Int): String? {
        return try {
            val pref = line!!.substring(0, index)
            val suff = line.substring(index, line.length)
            pref + character + suff
        } catch (e: Exception) {
            line
        }
    }

    override fun replaceAt(line: String?, character: String, start: Int, end: Int): String {
        var start = start
        var end = end
        start = Math.min(Math.max(0, start), line!!.length)
        end = Math.min(Math.max(0, end), line.length)
        val pref = line.substring(0, if (start > end) end else start)
        val suff = line.substring(if (start > end) start else end, line.length)
        return pref + character + suff
    }

    override fun getMousePos(
        text: String?,
        font: TrueTypeFont?,
        x: Float,
        mx: Int,
        scroll: Float
    ): Int {
        var before = -1
        var after = -1
        for (i in 0..text!!.length) {
            val wb = font!!.getWidth(text.substring(0, Math.max(i - 1, 0)))
            val wa = font.getWidth(text.substring(0, i))
            if (wa > mx - x - scroll) {
                before = wb
                after = wa
                break
            }
        }
        if (mx - x - scroll >= font!!.getWidth(text)) {
            after = font.getWidth(text)
        }
        var to =
            if (Math.abs(mx - x - scroll - before) < Math.abs(mx - x - scroll - after)) before else after
        for (i in 0 until text.length) {
            if (font.getWidth(text.substring(0, i)) == to) {
                to = i
                break
            }
        }
        if (to > text.length) {
            to = text.length
        }
        return to
    }

    override fun getChatFormatter(c: Char): Format {
        return if (c >= '0' && c <= '9' || c >= 'a' && c <= 'f') {
            Format.COLOR
        } else when (c) {
            'k' -> Format.FORMAT
            'l' -> Format.FORMAT
            'm' -> Format.FORMAT
            'n' -> Format.FORMAT
            'o' -> Format.FORMAT
            'r' -> Format.FORMAT
            else -> Format.NONE
        }
    }

    override fun getColorFromChar(c: Char): Color? {
        val chars =
            charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')
        val colors = intArrayOf(
            -0x1000000,
            -0xffff56,
            -0xff5600,
            -0xff5556,
            -0x560000,
            -0x55ff56,
            -0x5600,
            -0x555556,
            -0xaaaaab,
            -0xaaaa01,
            -0xaa00ab,
            -0xaa0001,
            -0xaaab,
            -0xaa01,
            -0xab,
            -0x10102
        )
        var color = -1
        for (i in chars.indices) {
            if (chars[i] == c) {
                color = colors[i]
                break
            }
        }
        return if (color == -1) {
            null
        } else Color(color)
    }

    override fun clearFormat(s: String?): String? {
        var s = s
        val formats: MutableList<String> = ArrayList()
        for (i in 0 until s!!.length) {
            val c = s[i]
            if (c == '\u00a7') {
                formats.add(s.substring(i, Math.min(i + 2, s.length)))
            }
        }
        for (st in formats) {
            s = s!!.replace(st, "")
        }
        return s
    }
}