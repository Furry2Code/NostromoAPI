package com.kilo.util

import com.nostromo.api.utils.ITimeConverter
import java.text.Format
import java.text.SimpleDateFormat
import java.util.*

class TimeConverter : ITimeConverter{
    override fun convertTime(time: Long): String {
        val date = Date(time)
        val format: Format = SimpleDateFormat("HH:mm:ss")
        return format.format(date)
    }
}