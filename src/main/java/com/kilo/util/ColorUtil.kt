package com.kilo.utilimport

import com.nostromo.api.utils.IColorUtil
import com.nostromo.api.utils.IColorUtil.colors
import java.awt.Color
import java.util.HashMap

class ColorUtil : IColorUtil {
    override  fun transparency(color: Int, alpha: Double): Int {
        val c = Color(color)
        val r = 0.003921569f * c.red
        val g = 0.003921569f * c.green
        val b = 0.003921569f * c.blue
        return Color(r, g, b, alpha.toFloat()).rgb
    }

    override fun rainbow(offset: Long, fade: Float): Color {
        val hue = (System.nanoTime() + offset) / 1.0E10f % 1.0f
        val color =
            Integer.toHexString(Integer.valueOf(Color.HSBtoRGB(hue, 1.0f, 1.0f)))
                .toLong(16)
        val c = Color(color.toInt())
        return Color(
            c.red / 255.0f * fade,
            c.green / 255.0f * fade,
            c.blue / 255.0f * fade,
            c.alpha / 255.0f
        )
    }

    override fun getRGBA(color: Int): FloatArray {
        val a = (color shr 24 and 0xFF) / 255.0f
        val r = (color shr 16 and 0xFF) / 255.0f
        val g = (color shr 8 and 0xFF) / 255.0f
        val b = (color and 0xFF) / 255.0f
        return floatArrayOf(r, g, b, a)
    }

    override  fun intFromHex(hex: String): Int {
        return try {
            if (hex.equals("rainbow", ignoreCase = true)) {
                rainbow(0L, 1.0f).rgb
            } else hex.toInt(16)
        } catch (e: NumberFormatException) {
            -1
        }
    }

    override fun hexFromInt(color: Int): String {
        return hexFromInt(Color(color))
    }

    fun hexFromInt(color: Color): String {
        return Integer.toHexString(color.rgb).substring(2)
    }

    override fun blend(color1: Color, color2: Color, ratio: Double): Color {
        val r = ratio.toFloat()
        val ir = 1.0f - r
        val rgb1 = FloatArray(3)
        val rgb2 = FloatArray(3)
        color1.getColorComponents(rgb1)
        color2.getColorComponents(rgb2)
        return Color(
            rgb1[0] * r + rgb2[0] * ir,
            rgb1[1] * r + rgb2[1] * ir,
            rgb1[2] * r + rgb2[2] * ir
        )
    }

    override fun darker(color: Color, fraction: Double): Color {
        var red = Math.round(color.red * (1.0 - fraction)).toInt()
        var green = Math.round(color.green * (1.0 - fraction)).toInt()
        var blue = Math.round(color.blue * (1.0 - fraction)).toInt()
        if (red < 0) {
            red = 0
        } else if (red > 255) {
            red = 255
        }
        if (green < 0) {
            green = 0
        } else if (green > 255) {
            green = 255
        }
        if (blue < 0) {
            blue = 0
        } else if (blue > 255) {
            blue = 255
        }
        val alpha = color.alpha
        return Color(red, green, blue, alpha)
    }

    override fun lighter(color: Color, fraction: Double): Color {
        var red = Math.round(color.red * (1.0 + fraction)).toInt()
        var green = Math.round(color.green * (1.0 + fraction)).toInt()
        var blue = Math.round(color.blue * (1.0 + fraction)).toInt()
        if (red < 0) {
            red = 0
        } else if (red > 255) {
            red = 255
        }
        if (green < 0) {
            green = 0
        } else if (green > 255) {
            green = 255
        }
        if (blue < 0) {
            blue = 0
        } else if (blue > 255) {
            blue = 255
        }
        val alpha = color.alpha
        return Color(red, green, blue, alpha)
    }

    override fun getHexName(color: Color): String {
        val r = color.red
        val g = color.green
        val b = color.blue
        val rHex = Integer.toString(r, 16)
        val gHex = Integer.toString(g, 16)
        val bHex = Integer.toString(b, 16)
        return if (rHex.length == 2) StringBuilder().append(rHex)
            .toString() else StringBuilder("0").append(rHex).toString() + (if (gHex.length == 2) StringBuilder().append(
            gHex
        )
            .toString() else "0$gHex") + if (bHex.length == 2) StringBuilder().append(bHex)
            .toString() else "0$bHex"
    }

    override fun colorDistance(
        r1: Double,
        g1: Double,
        b1: Double,
        r2: Double,
        g2: Double,
        b2: Double
    ): Double {
        val a = r2 - r1
        val b3 = g2 - g1
        val c = b2 - b1
        return Math.sqrt(a * a + b3 * b3 + c * c)
    }

    override fun colorDistance(color1: DoubleArray, color2: DoubleArray): Double {
        return colorDistance(
            color1[0],
            color1[1],
            color1[2],
            color2[0],
            color2[1],
            color2[2]
        )
    }

    override  fun colorDistance(color1: Color, color2: Color): Double {
        val rgb1 = FloatArray(3)
        val rgb2 = FloatArray(3)
        color1.getColorComponents(rgb1)
        color2.getColorComponents(rgb2)
        return colorDistance(
            rgb1[0].toDouble(), rgb1[1].toDouble(), rgb1[2].toDouble(), rgb2[0].toDouble(), rgb2[1]
                .toDouble(), rgb2[2].toDouble()
        )
    }

    override fun isDark(r: Double, g: Double, b: Double): Boolean {
        val dWhite = colorDistance(r, g, b, 1.0, 1.0, 1.0)
        val dBlack = colorDistance(r, g, b, 0.0, 0.0, 0.0)
        return dBlack < dWhite
    }

    override  fun isDark(color: Color): Boolean {
        val r = color.red / 255.0f
        val g = color.green / 255.0f
        val b = color.blue / 255.0f
        return isDark(r.toDouble(), g.toDouble(), b.toDouble())
    }

    override fun getColors() : HashMap<String, ChatColor>? {
        return colors
    }
}