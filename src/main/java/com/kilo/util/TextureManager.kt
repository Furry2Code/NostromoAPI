package com.kilo.managerimport

import com.kilo.API
import com.kilo.render.TextureImage
import com.nostromo.api.utils.ITextureManager
import java.util.concurrent.CopyOnWriteArrayList


class TextureManager : ITextureManager {
    var cache: List<TextureImage> = CopyOnWriteArrayList<TextureImage>()
    override fun exists(location: String?): Boolean {
        return this.get(location) != null
    }

    override operator fun get(location: String?): TextureImage? {
        for (ti in this.cache) {
            if (ti.location.equals(location, ignoreCase = true)) {
                return ti
            }
        }
        return null
    }

    override fun getCaches(): MutableList<TextureImage> {
        return caches
    }
}