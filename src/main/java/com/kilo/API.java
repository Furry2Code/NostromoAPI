package com.kilo;

import com.kilo.encryption.CipherFiles;
import com.kilo.inputimport.IngameInput;
import com.kilo.inputimport.Input;
import com.kilo.managerimport.TextureManager;
import com.kilo.render.Draw;
import com.kilo.util.*;
import com.kilo.utilimport.*;
import com.nostromo.api.encryption.ICipherFiles;
import com.nostromo.api.input.IIngameInput;
import com.nostromo.api.input.IInput;
import com.nostromo.api.render.IDraw;
import com.nostromo.api.utils.*;

public class API {
    public static ICipherFiles cipherFiles = new CipherFiles();
    public static IIngameInput ingameInput = new IngameInput();
    public static IInput input = new Input();
    public static IDraw draw = new Draw();
    public static IUtil util = new Util();
    public static IChatUtil chatUtil = new ChatUtil();
    public static IClientUtils clientUtils = new ClientUtils();
    public static IColorUtil colorUtil = new ColorUtil();
    public static ICryptPass cryptPass = new CryptPass();
    public static IFileUtils fileUtils = new FileUtils();
    public static IJTextureManager ijTextureManager = new JTextureManager();
    public static IRenderUtil renderUtil = new RenderUtil();
    public static IKiloCapeUtils kiloCapeUtils = new KiloCapeUtils();
    public static IResources resources = new Resources();
    public static IRotationUtils rotationUtils = new RotationUtils();
    public static ITextureManager textureManager = new TextureManager();
    public static ITimeConverter timeConverter = new TimeConverter();
    public static ITimer timer = new Timer();
    public static ITimerUtil timerUtil = new TimerUtil();
    public static IUUIDUtil iuuidUtil = new UUIDUtil();

    public static IChatUtil getChatUtil() {
        return chatUtil;
    }

    public static ICipherFiles getCipherFiles() {
        return cipherFiles;
    }

    public static IClientUtils getClientUtils() {
        return clientUtils;
    }

    public static IColorUtil getColorUtil() {
        return colorUtil;
    }

    public static ICryptPass getCryptPass() {
        return cryptPass;
    }

    public static IDraw getDraw() {
        return draw;
    }

    public static IFileUtils getFileUtils() {
        return fileUtils;
    }

    public static IIngameInput getIngameInput() {
        return ingameInput;
    }

    public static IInput getInput() {
        return input;
    }

    public static IJTextureManager getIjTextureManager() {
        return ijTextureManager;
    }

    public static IKiloCapeUtils getKiloCapeUtils() {
        return kiloCapeUtils;
    }

    public static IRenderUtil getRenderUtil() {
        return renderUtil;
    }

    public static IResources getResources() {
        return resources;
    }

    public static IRotationUtils getRotationUtils() {
        return rotationUtils;
    }

    public static ITextureManager getTextureManager() {
        return textureManager;
    }

    public static ITimeConverter getTimeConverter() {
        return timeConverter;
    }

    public static ITimer getTimer() {
        return timer;
    }

    public static IUtil getUtil() {
        return util;
    }

    public static ITimerUtil getTimerUtil() {
        return timerUtil;
    }

    public static IUUIDUtil getIuuidUtil() {
        return iuuidUtil;
    }
}
